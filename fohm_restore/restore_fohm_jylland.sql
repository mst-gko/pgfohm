
    BEGIN;
        
        CREATE SCHEMA IF NOT EXISTS mstfohm;
        
        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON SCHEMA mstfohm IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: all grids from fohm'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
    
        /*
        * table:jylland_0000_terraen
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_0000_terraen";
        
        CREATE TABLE mstfohm."jylland_0000_terraen" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 0
            )
        ;
        
        ALTER TABLE mstfohm."jylland_0000_terraen" DROP CONSTRAINT IF EXISTS "jylland_0000_terraen_pkey";
        ALTER TABLE mstfohm."jylland_0000_terraen" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0000_terraen_rastidx";
        CREATE INDEX "jylland_0000_terraen_rastidx" ON mstfohm."jylland_0000_terraen" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0000_terraen" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_0000_terraen from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0000_terraen'::name,'rast'::name);
        
        /*
        * table:jylland_0050_antropocaen
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_0050_antropocaen";
        
        CREATE TABLE mstfohm."jylland_0050_antropocaen" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 50
            )
        ;
        
        ALTER TABLE mstfohm."jylland_0050_antropocaen" DROP CONSTRAINT IF EXISTS "jylland_0050_antropocaen_pkey";
        ALTER TABLE mstfohm."jylland_0050_antropocaen" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0050_antropocaen_rastidx";
        CREATE INDEX "jylland_0050_antropocaen_rastidx" ON mstfohm."jylland_0050_antropocaen" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0050_antropocaen" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_0050_antropocaen from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0050_antropocaen'::name,'rast'::name);
        
        /*
        * table:jylland_0100_postglacial_toerv
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_0100_postglacial_toerv";
        
        CREATE TABLE mstfohm."jylland_0100_postglacial_toerv" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 100
            )
        ;
        
        ALTER TABLE mstfohm."jylland_0100_postglacial_toerv" DROP CONSTRAINT IF EXISTS "jylland_0100_postglacial_toerv_pkey";
        ALTER TABLE mstfohm."jylland_0100_postglacial_toerv" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0100_postglacial_toerv_rastidx";
        CREATE INDEX "jylland_0100_postglacial_toerv_rastidx" ON mstfohm."jylland_0100_postglacial_toerv" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0100_postglacial_toerv" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_0100_postglacial_toerv from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0100_postglacial_toerv'::name,'rast'::name);
        
        /*
        * table:jylland_0120_postglacial_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_0120_postglacial_sand";
        
        CREATE TABLE mstfohm."jylland_0120_postglacial_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 120
            )
        ;
        
        ALTER TABLE mstfohm."jylland_0120_postglacial_sand" DROP CONSTRAINT IF EXISTS "jylland_0120_postglacial_sand_pkey";
        ALTER TABLE mstfohm."jylland_0120_postglacial_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0120_postglacial_sand_rastidx";
        CREATE INDEX "jylland_0120_postglacial_sand_rastidx" ON mstfohm."jylland_0120_postglacial_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0120_postglacial_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_0120_postglacial_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0120_postglacial_sand'::name,'rast'::name);
        
        /*
        * table:jylland_0130_postglacial_littorinaler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_0130_postglacial_littorinaler";
        
        CREATE TABLE mstfohm."jylland_0130_postglacial_littorinaler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 130
            )
        ;
        
        ALTER TABLE mstfohm."jylland_0130_postglacial_littorinaler" DROP CONSTRAINT IF EXISTS "jylland_0130_postglacial_littorinaler_pkey";
        ALTER TABLE mstfohm."jylland_0130_postglacial_littorinaler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0130_postglacial_littorinaler_rastidx";
        CREATE INDEX "jylland_0130_postglacial_littorinaler_rastidx" ON mstfohm."jylland_0130_postglacial_littorinaler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0130_postglacial_littorinaler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_0130_postglacial_littorinaler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0130_postglacial_littorinaler'::name,'rast'::name);
        
        /*
        * table:jylland_0140_senglacial_yoldiasand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_0140_senglacial_yoldiasand";
        
        CREATE TABLE mstfohm."jylland_0140_senglacial_yoldiasand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 140
            )
        ;
        
        ALTER TABLE mstfohm."jylland_0140_senglacial_yoldiasand" DROP CONSTRAINT IF EXISTS "jylland_0140_senglacial_yoldiasand_pkey";
        ALTER TABLE mstfohm."jylland_0140_senglacial_yoldiasand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0140_senglacial_yoldiasand_rastidx";
        CREATE INDEX "jylland_0140_senglacial_yoldiasand_rastidx" ON mstfohm."jylland_0140_senglacial_yoldiasand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0140_senglacial_yoldiasand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_0140_senglacial_yoldiasand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0140_senglacial_yoldiasand'::name,'rast'::name);
        
        /*
        * table:jylland_0150_senglacial_yoldialer
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_0150_senglacial_yoldialer";
        
        CREATE TABLE mstfohm."jylland_0150_senglacial_yoldialer" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 150
            )
        ;
        
        ALTER TABLE mstfohm."jylland_0150_senglacial_yoldialer" DROP CONSTRAINT IF EXISTS "jylland_0150_senglacial_yoldialer_pkey";
        ALTER TABLE mstfohm."jylland_0150_senglacial_yoldialer" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0150_senglacial_yoldialer_rastidx";
        CREATE INDEX "jylland_0150_senglacial_yoldialer_rastidx" ON mstfohm."jylland_0150_senglacial_yoldialer" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0150_senglacial_yoldialer" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_0150_senglacial_yoldialer from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0150_senglacial_yoldialer'::name,'rast'::name);
        
        /*
        * table:jylland_0200_kvartaer_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_0200_kvartaer_sand";
        
        CREATE TABLE mstfohm."jylland_0200_kvartaer_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 200
            )
        ;
        
        ALTER TABLE mstfohm."jylland_0200_kvartaer_sand" DROP CONSTRAINT IF EXISTS "jylland_0200_kvartaer_sand_pkey";
        ALTER TABLE mstfohm."jylland_0200_kvartaer_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0200_kvartaer_sand_rastidx";
        CREATE INDEX "jylland_0200_kvartaer_sand_rastidx" ON mstfohm."jylland_0200_kvartaer_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0200_kvartaer_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_0200_kvartaer_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0200_kvartaer_sand'::name,'rast'::name);
        
        /*
        * table:jylland_0300_kvartaer_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_0300_kvartaer_ler";
        
        CREATE TABLE mstfohm."jylland_0300_kvartaer_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 300
            )
        ;
        
        ALTER TABLE mstfohm."jylland_0300_kvartaer_ler" DROP CONSTRAINT IF EXISTS "jylland_0300_kvartaer_ler_pkey";
        ALTER TABLE mstfohm."jylland_0300_kvartaer_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0300_kvartaer_ler_rastidx";
        CREATE INDEX "jylland_0300_kvartaer_ler_rastidx" ON mstfohm."jylland_0300_kvartaer_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0300_kvartaer_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_0300_kvartaer_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0300_kvartaer_ler'::name,'rast'::name);
        
        /*
        * table:jylland_0400_kvartaer_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_0400_kvartaer_sand";
        
        CREATE TABLE mstfohm."jylland_0400_kvartaer_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 400
            )
        ;
        
        ALTER TABLE mstfohm."jylland_0400_kvartaer_sand" DROP CONSTRAINT IF EXISTS "jylland_0400_kvartaer_sand_pkey";
        ALTER TABLE mstfohm."jylland_0400_kvartaer_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0400_kvartaer_sand_rastidx";
        CREATE INDEX "jylland_0400_kvartaer_sand_rastidx" ON mstfohm."jylland_0400_kvartaer_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0400_kvartaer_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_0400_kvartaer_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0400_kvartaer_sand'::name,'rast'::name);
        
        /*
        * table:jylland_1100_kvartaer_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_1100_kvartaer_ler";
        
        CREATE TABLE mstfohm."jylland_1100_kvartaer_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 1100
            )
        ;
        
        ALTER TABLE mstfohm."jylland_1100_kvartaer_ler" DROP CONSTRAINT IF EXISTS "jylland_1100_kvartaer_ler_pkey";
        ALTER TABLE mstfohm."jylland_1100_kvartaer_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_1100_kvartaer_ler_rastidx";
        CREATE INDEX "jylland_1100_kvartaer_ler_rastidx" ON mstfohm."jylland_1100_kvartaer_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_1100_kvartaer_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_1100_kvartaer_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_1100_kvartaer_ler'::name,'rast'::name);
        
        /*
        * table:jylland_1200_kvartaer_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_1200_kvartaer_sand";
        
        CREATE TABLE mstfohm."jylland_1200_kvartaer_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 1200
            )
        ;
        
        ALTER TABLE mstfohm."jylland_1200_kvartaer_sand" DROP CONSTRAINT IF EXISTS "jylland_1200_kvartaer_sand_pkey";
        ALTER TABLE mstfohm."jylland_1200_kvartaer_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_1200_kvartaer_sand_rastidx";
        CREATE INDEX "jylland_1200_kvartaer_sand_rastidx" ON mstfohm."jylland_1200_kvartaer_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_1200_kvartaer_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_1200_kvartaer_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_1200_kvartaer_sand'::name,'rast'::name);
        
        /*
        * table:jylland_1300_kvartaer_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_1300_kvartaer_ler";
        
        CREATE TABLE mstfohm."jylland_1300_kvartaer_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 1300
            )
        ;
        
        ALTER TABLE mstfohm."jylland_1300_kvartaer_ler" DROP CONSTRAINT IF EXISTS "jylland_1300_kvartaer_ler_pkey";
        ALTER TABLE mstfohm."jylland_1300_kvartaer_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_1300_kvartaer_ler_rastidx";
        CREATE INDEX "jylland_1300_kvartaer_ler_rastidx" ON mstfohm."jylland_1300_kvartaer_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_1300_kvartaer_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_1300_kvartaer_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_1300_kvartaer_ler'::name,'rast'::name);
        
        /*
        * table:jylland_1400_kvartaer_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_1400_kvartaer_sand";
        
        CREATE TABLE mstfohm."jylland_1400_kvartaer_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 1400
            )
        ;
        
        ALTER TABLE mstfohm."jylland_1400_kvartaer_sand" DROP CONSTRAINT IF EXISTS "jylland_1400_kvartaer_sand_pkey";
        ALTER TABLE mstfohm."jylland_1400_kvartaer_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_1400_kvartaer_sand_rastidx";
        CREATE INDEX "jylland_1400_kvartaer_sand_rastidx" ON mstfohm."jylland_1400_kvartaer_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_1400_kvartaer_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_1400_kvartaer_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_1400_kvartaer_sand'::name,'rast'::name);
        
        /*
        * table:jylland_1500_kvartaer_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_1500_kvartaer_ler";
        
        CREATE TABLE mstfohm."jylland_1500_kvartaer_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 1500
            )
        ;
        
        ALTER TABLE mstfohm."jylland_1500_kvartaer_ler" DROP CONSTRAINT IF EXISTS "jylland_1500_kvartaer_ler_pkey";
        ALTER TABLE mstfohm."jylland_1500_kvartaer_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_1500_kvartaer_ler_rastidx";
        CREATE INDEX "jylland_1500_kvartaer_ler_rastidx" ON mstfohm."jylland_1500_kvartaer_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_1500_kvartaer_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_1500_kvartaer_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_1500_kvartaer_ler'::name,'rast'::name);
        
        /*
        * table:jylland_2100_kvartaer_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_2100_kvartaer_sand";
        
        CREATE TABLE mstfohm."jylland_2100_kvartaer_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 2100
            )
        ;
        
        ALTER TABLE mstfohm."jylland_2100_kvartaer_sand" DROP CONSTRAINT IF EXISTS "jylland_2100_kvartaer_sand_pkey";
        ALTER TABLE mstfohm."jylland_2100_kvartaer_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_2100_kvartaer_sand_rastidx";
        CREATE INDEX "jylland_2100_kvartaer_sand_rastidx" ON mstfohm."jylland_2100_kvartaer_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_2100_kvartaer_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_2100_kvartaer_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_2100_kvartaer_sand'::name,'rast'::name);
        
        /*
        * table:jylland_2200_kvartaer_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_2200_kvartaer_ler";
        
        CREATE TABLE mstfohm."jylland_2200_kvartaer_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 2200
            )
        ;
        
        ALTER TABLE mstfohm."jylland_2200_kvartaer_ler" DROP CONSTRAINT IF EXISTS "jylland_2200_kvartaer_ler_pkey";
        ALTER TABLE mstfohm."jylland_2200_kvartaer_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_2200_kvartaer_ler_rastidx";
        CREATE INDEX "jylland_2200_kvartaer_ler_rastidx" ON mstfohm."jylland_2200_kvartaer_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_2200_kvartaer_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_2200_kvartaer_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_2200_kvartaer_ler'::name,'rast'::name);
        
        /*
        * table:jylland_2300_kvartaer_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_2300_kvartaer_sand";
        
        CREATE TABLE mstfohm."jylland_2300_kvartaer_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 2300
            )
        ;
        
        ALTER TABLE mstfohm."jylland_2300_kvartaer_sand" DROP CONSTRAINT IF EXISTS "jylland_2300_kvartaer_sand_pkey";
        ALTER TABLE mstfohm."jylland_2300_kvartaer_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_2300_kvartaer_sand_rastidx";
        CREATE INDEX "jylland_2300_kvartaer_sand_rastidx" ON mstfohm."jylland_2300_kvartaer_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_2300_kvartaer_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_2300_kvartaer_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_2300_kvartaer_sand'::name,'rast'::name);
        
        /*
        * table:jylland_2400_kvartaer_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_2400_kvartaer_ler";
        
        CREATE TABLE mstfohm."jylland_2400_kvartaer_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 2400
            )
        ;
        
        ALTER TABLE mstfohm."jylland_2400_kvartaer_ler" DROP CONSTRAINT IF EXISTS "jylland_2400_kvartaer_ler_pkey";
        ALTER TABLE mstfohm."jylland_2400_kvartaer_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_2400_kvartaer_ler_rastidx";
        CREATE INDEX "jylland_2400_kvartaer_ler_rastidx" ON mstfohm."jylland_2400_kvartaer_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_2400_kvartaer_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_2400_kvartaer_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_2400_kvartaer_ler'::name,'rast'::name);
        
        /*
        * table:jylland_5100_maadegruppen_gram_og_hodde
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_5100_maadegruppen_gram_og_hodde";
        
        CREATE TABLE mstfohm."jylland_5100_maadegruppen_gram_og_hodde" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 5100
            )
        ;
        
        ALTER TABLE mstfohm."jylland_5100_maadegruppen_gram_og_hodde" DROP CONSTRAINT IF EXISTS "jylland_5100_maadegruppen_gram_og_hodde_pkey";
        ALTER TABLE mstfohm."jylland_5100_maadegruppen_gram_og_hodde" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5100_maadegruppen_gram_og_hodde_rastidx";
        CREATE INDEX "jylland_5100_maadegruppen_gram_og_hodde_rastidx" ON mstfohm."jylland_5100_maadegruppen_gram_og_hodde" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5100_maadegruppen_gram_og_hodde" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_5100_maadegruppen_gram_og_hodde from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5100_maadegruppen_gram_og_hodde'::name,'rast'::name);
        
        /*
        * table:jylland_5200_øvre_odderup_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_5200_øvre_odderup_sand";
        
        CREATE TABLE mstfohm."jylland_5200_øvre_odderup_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 5200
            )
        ;
        
        ALTER TABLE mstfohm."jylland_5200_øvre_odderup_sand" DROP CONSTRAINT IF EXISTS "jylland_5200_øvre_odderup_sand_pkey";
        ALTER TABLE mstfohm."jylland_5200_øvre_odderup_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5200_øvre_odderup_sand_rastidx";
        CREATE INDEX "jylland_5200_øvre_odderup_sand_rastidx" ON mstfohm."jylland_5200_øvre_odderup_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5200_øvre_odderup_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_5200_øvre_odderup_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5200_øvre_odderup_sand'::name,'rast'::name);
        
        /*
        * table:jylland_5300_øvre_arnum_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_5300_øvre_arnum_ler";
        
        CREATE TABLE mstfohm."jylland_5300_øvre_arnum_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 5300
            )
        ;
        
        ALTER TABLE mstfohm."jylland_5300_øvre_arnum_ler" DROP CONSTRAINT IF EXISTS "jylland_5300_øvre_arnum_ler_pkey";
        ALTER TABLE mstfohm."jylland_5300_øvre_arnum_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5300_øvre_arnum_ler_rastidx";
        CREATE INDEX "jylland_5300_øvre_arnum_ler_rastidx" ON mstfohm."jylland_5300_øvre_arnum_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5300_øvre_arnum_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_5300_øvre_arnum_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5300_øvre_arnum_ler'::name,'rast'::name);
        
        /*
        * table:jylland_5400_nedre_odderup_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_5400_nedre_odderup_sand";
        
        CREATE TABLE mstfohm."jylland_5400_nedre_odderup_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 5400
            )
        ;
        
        ALTER TABLE mstfohm."jylland_5400_nedre_odderup_sand" DROP CONSTRAINT IF EXISTS "jylland_5400_nedre_odderup_sand_pkey";
        ALTER TABLE mstfohm."jylland_5400_nedre_odderup_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5400_nedre_odderup_sand_rastidx";
        CREATE INDEX "jylland_5400_nedre_odderup_sand_rastidx" ON mstfohm."jylland_5400_nedre_odderup_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5400_nedre_odderup_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_5400_nedre_odderup_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5400_nedre_odderup_sand'::name,'rast'::name);
        
        /*
        * table:jylland_5500_nedre_arnum_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_5500_nedre_arnum_ler";
        
        CREATE TABLE mstfohm."jylland_5500_nedre_arnum_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 5500
            )
        ;
        
        ALTER TABLE mstfohm."jylland_5500_nedre_arnum_ler" DROP CONSTRAINT IF EXISTS "jylland_5500_nedre_arnum_ler_pkey";
        ALTER TABLE mstfohm."jylland_5500_nedre_arnum_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5500_nedre_arnum_ler_rastidx";
        CREATE INDEX "jylland_5500_nedre_arnum_ler_rastidx" ON mstfohm."jylland_5500_nedre_arnum_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5500_nedre_arnum_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_5500_nedre_arnum_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5500_nedre_arnum_ler'::name,'rast'::name);
        
        /*
        * table:jylland_5600_bastrup_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_5600_bastrup_sand";
        
        CREATE TABLE mstfohm."jylland_5600_bastrup_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 5600
            )
        ;
        
        ALTER TABLE mstfohm."jylland_5600_bastrup_sand" DROP CONSTRAINT IF EXISTS "jylland_5600_bastrup_sand_pkey";
        ALTER TABLE mstfohm."jylland_5600_bastrup_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5600_bastrup_sand_rastidx";
        CREATE INDEX "jylland_5600_bastrup_sand_rastidx" ON mstfohm."jylland_5600_bastrup_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5600_bastrup_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_5600_bastrup_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5600_bastrup_sand'::name,'rast'::name);
        
        /*
        * table:jylland_5700_klintinghoved_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_5700_klintinghoved_ler";
        
        CREATE TABLE mstfohm."jylland_5700_klintinghoved_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 5700
            )
        ;
        
        ALTER TABLE mstfohm."jylland_5700_klintinghoved_ler" DROP CONSTRAINT IF EXISTS "jylland_5700_klintinghoved_ler_pkey";
        ALTER TABLE mstfohm."jylland_5700_klintinghoved_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5700_klintinghoved_ler_rastidx";
        CREATE INDEX "jylland_5700_klintinghoved_ler_rastidx" ON mstfohm."jylland_5700_klintinghoved_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5700_klintinghoved_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_5700_klintinghoved_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5700_klintinghoved_ler'::name,'rast'::name);
        
        /*
        * table:jylland_5800_bastrup_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_5800_bastrup_sand";
        
        CREATE TABLE mstfohm."jylland_5800_bastrup_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 5800
            )
        ;
        
        ALTER TABLE mstfohm."jylland_5800_bastrup_sand" DROP CONSTRAINT IF EXISTS "jylland_5800_bastrup_sand_pkey";
        ALTER TABLE mstfohm."jylland_5800_bastrup_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5800_bastrup_sand_rastidx";
        CREATE INDEX "jylland_5800_bastrup_sand_rastidx" ON mstfohm."jylland_5800_bastrup_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5800_bastrup_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_5800_bastrup_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5800_bastrup_sand'::name,'rast'::name);
        
        /*
        * table:jylland_5900_klintinghoved_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_5900_klintinghoved_ler";
        
        CREATE TABLE mstfohm."jylland_5900_klintinghoved_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 5900
            )
        ;
        
        ALTER TABLE mstfohm."jylland_5900_klintinghoved_ler" DROP CONSTRAINT IF EXISTS "jylland_5900_klintinghoved_ler_pkey";
        ALTER TABLE mstfohm."jylland_5900_klintinghoved_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5900_klintinghoved_ler_rastidx";
        CREATE INDEX "jylland_5900_klintinghoved_ler_rastidx" ON mstfohm."jylland_5900_klintinghoved_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5900_klintinghoved_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_5900_klintinghoved_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5900_klintinghoved_ler'::name,'rast'::name);
        
        /*
        * table:jylland_6000_bastrup_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_6000_bastrup_sand";
        
        CREATE TABLE mstfohm."jylland_6000_bastrup_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 6000
            )
        ;
        
        ALTER TABLE mstfohm."jylland_6000_bastrup_sand" DROP CONSTRAINT IF EXISTS "jylland_6000_bastrup_sand_pkey";
        ALTER TABLE mstfohm."jylland_6000_bastrup_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6000_bastrup_sand_rastidx";
        CREATE INDEX "jylland_6000_bastrup_sand_rastidx" ON mstfohm."jylland_6000_bastrup_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6000_bastrup_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_6000_bastrup_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6000_bastrup_sand'::name,'rast'::name);
        
        /*
        * table:jylland_6100_klintinghoved_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_6100_klintinghoved_ler";
        
        CREATE TABLE mstfohm."jylland_6100_klintinghoved_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 6100
            )
        ;
        
        ALTER TABLE mstfohm."jylland_6100_klintinghoved_ler" DROP CONSTRAINT IF EXISTS "jylland_6100_klintinghoved_ler_pkey";
        ALTER TABLE mstfohm."jylland_6100_klintinghoved_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6100_klintinghoved_ler_rastidx";
        CREATE INDEX "jylland_6100_klintinghoved_ler_rastidx" ON mstfohm."jylland_6100_klintinghoved_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6100_klintinghoved_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_6100_klintinghoved_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6100_klintinghoved_ler'::name,'rast'::name);
        
        /*
        * table:jylland_6200_bastrup_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_6200_bastrup_sand";
        
        CREATE TABLE mstfohm."jylland_6200_bastrup_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 6200
            )
        ;
        
        ALTER TABLE mstfohm."jylland_6200_bastrup_sand" DROP CONSTRAINT IF EXISTS "jylland_6200_bastrup_sand_pkey";
        ALTER TABLE mstfohm."jylland_6200_bastrup_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6200_bastrup_sand_rastidx";
        CREATE INDEX "jylland_6200_bastrup_sand_rastidx" ON mstfohm."jylland_6200_bastrup_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6200_bastrup_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_6200_bastrup_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6200_bastrup_sand'::name,'rast'::name);
        
        /*
        * table:jylland_6300_klintinghoved_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_6300_klintinghoved_ler";
        
        CREATE TABLE mstfohm."jylland_6300_klintinghoved_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 6300
            )
        ;
        
        ALTER TABLE mstfohm."jylland_6300_klintinghoved_ler" DROP CONSTRAINT IF EXISTS "jylland_6300_klintinghoved_ler_pkey";
        ALTER TABLE mstfohm."jylland_6300_klintinghoved_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6300_klintinghoved_ler_rastidx";
        CREATE INDEX "jylland_6300_klintinghoved_ler_rastidx" ON mstfohm."jylland_6300_klintinghoved_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6300_klintinghoved_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_6300_klintinghoved_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6300_klintinghoved_ler'::name,'rast'::name);
        
        /*
        * table:jylland_6400_bastrup_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_6400_bastrup_sand";
        
        CREATE TABLE mstfohm."jylland_6400_bastrup_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 6400
            )
        ;
        
        ALTER TABLE mstfohm."jylland_6400_bastrup_sand" DROP CONSTRAINT IF EXISTS "jylland_6400_bastrup_sand_pkey";
        ALTER TABLE mstfohm."jylland_6400_bastrup_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6400_bastrup_sand_rastidx";
        CREATE INDEX "jylland_6400_bastrup_sand_rastidx" ON mstfohm."jylland_6400_bastrup_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6400_bastrup_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_6400_bastrup_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6400_bastrup_sand'::name,'rast'::name);
        
        /*
        * table:jylland_6500_klintinghoved_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_6500_klintinghoved_ler";
        
        CREATE TABLE mstfohm."jylland_6500_klintinghoved_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 6500
            )
        ;
        
        ALTER TABLE mstfohm."jylland_6500_klintinghoved_ler" DROP CONSTRAINT IF EXISTS "jylland_6500_klintinghoved_ler_pkey";
        ALTER TABLE mstfohm."jylland_6500_klintinghoved_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6500_klintinghoved_ler_rastidx";
        CREATE INDEX "jylland_6500_klintinghoved_ler_rastidx" ON mstfohm."jylland_6500_klintinghoved_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6500_klintinghoved_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_6500_klintinghoved_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6500_klintinghoved_ler'::name,'rast'::name);
        
        /*
        * table:jylland_6600_bastrup_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_6600_bastrup_sand";
        
        CREATE TABLE mstfohm."jylland_6600_bastrup_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 6600
            )
        ;
        
        ALTER TABLE mstfohm."jylland_6600_bastrup_sand" DROP CONSTRAINT IF EXISTS "jylland_6600_bastrup_sand_pkey";
        ALTER TABLE mstfohm."jylland_6600_bastrup_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6600_bastrup_sand_rastidx";
        CREATE INDEX "jylland_6600_bastrup_sand_rastidx" ON mstfohm."jylland_6600_bastrup_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6600_bastrup_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_6600_bastrup_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6600_bastrup_sand'::name,'rast'::name);
        
        /*
        * table:jylland_6700_klintinghoved_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_6700_klintinghoved_ler";
        
        CREATE TABLE mstfohm."jylland_6700_klintinghoved_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 6700
            )
        ;
        
        ALTER TABLE mstfohm."jylland_6700_klintinghoved_ler" DROP CONSTRAINT IF EXISTS "jylland_6700_klintinghoved_ler_pkey";
        ALTER TABLE mstfohm."jylland_6700_klintinghoved_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6700_klintinghoved_ler_rastidx";
        CREATE INDEX "jylland_6700_klintinghoved_ler_rastidx" ON mstfohm."jylland_6700_klintinghoved_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6700_klintinghoved_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_6700_klintinghoved_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6700_klintinghoved_ler'::name,'rast'::name);
        
        /*
        * table:jylland_6800_billund_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_6800_billund_sand";
        
        CREATE TABLE mstfohm."jylland_6800_billund_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 6800
            )
        ;
        
        ALTER TABLE mstfohm."jylland_6800_billund_sand" DROP CONSTRAINT IF EXISTS "jylland_6800_billund_sand_pkey";
        ALTER TABLE mstfohm."jylland_6800_billund_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6800_billund_sand_rastidx";
        CREATE INDEX "jylland_6800_billund_sand_rastidx" ON mstfohm."jylland_6800_billund_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6800_billund_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_6800_billund_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6800_billund_sand'::name,'rast'::name);
        
        /*
        * table:jylland_6900_vejle_fjord_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_6900_vejle_fjord_ler";
        
        CREATE TABLE mstfohm."jylland_6900_vejle_fjord_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 6900
            )
        ;
        
        ALTER TABLE mstfohm."jylland_6900_vejle_fjord_ler" DROP CONSTRAINT IF EXISTS "jylland_6900_vejle_fjord_ler_pkey";
        ALTER TABLE mstfohm."jylland_6900_vejle_fjord_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6900_vejle_fjord_ler_rastidx";
        CREATE INDEX "jylland_6900_vejle_fjord_ler_rastidx" ON mstfohm."jylland_6900_vejle_fjord_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6900_vejle_fjord_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_6900_vejle_fjord_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6900_vejle_fjord_ler'::name,'rast'::name);
        
        /*
        * table:jylland_7000_billund_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_7000_billund_sand";
        
        CREATE TABLE mstfohm."jylland_7000_billund_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 7000
            )
        ;
        
        ALTER TABLE mstfohm."jylland_7000_billund_sand" DROP CONSTRAINT IF EXISTS "jylland_7000_billund_sand_pkey";
        ALTER TABLE mstfohm."jylland_7000_billund_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7000_billund_sand_rastidx";
        CREATE INDEX "jylland_7000_billund_sand_rastidx" ON mstfohm."jylland_7000_billund_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7000_billund_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_7000_billund_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7000_billund_sand'::name,'rast'::name);
        
        /*
        * table:jylland_7100_vejle_fjord_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_7100_vejle_fjord_ler";
        
        CREATE TABLE mstfohm."jylland_7100_vejle_fjord_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 7100
            )
        ;
        
        ALTER TABLE mstfohm."jylland_7100_vejle_fjord_ler" DROP CONSTRAINT IF EXISTS "jylland_7100_vejle_fjord_ler_pkey";
        ALTER TABLE mstfohm."jylland_7100_vejle_fjord_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7100_vejle_fjord_ler_rastidx";
        CREATE INDEX "jylland_7100_vejle_fjord_ler_rastidx" ON mstfohm."jylland_7100_vejle_fjord_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7100_vejle_fjord_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_7100_vejle_fjord_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7100_vejle_fjord_ler'::name,'rast'::name);
        
        /*
        * table:jylland_7200_billund_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_7200_billund_sand";
        
        CREATE TABLE mstfohm."jylland_7200_billund_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 7200
            )
        ;
        
        ALTER TABLE mstfohm."jylland_7200_billund_sand" DROP CONSTRAINT IF EXISTS "jylland_7200_billund_sand_pkey";
        ALTER TABLE mstfohm."jylland_7200_billund_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7200_billund_sand_rastidx";
        CREATE INDEX "jylland_7200_billund_sand_rastidx" ON mstfohm."jylland_7200_billund_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7200_billund_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_7200_billund_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7200_billund_sand'::name,'rast'::name);
        
        /*
        * table:jylland_7300_vejle_fjord_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_7300_vejle_fjord_ler";
        
        CREATE TABLE mstfohm."jylland_7300_vejle_fjord_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 7300
            )
        ;
        
        ALTER TABLE mstfohm."jylland_7300_vejle_fjord_ler" DROP CONSTRAINT IF EXISTS "jylland_7300_vejle_fjord_ler_pkey";
        ALTER TABLE mstfohm."jylland_7300_vejle_fjord_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7300_vejle_fjord_ler_rastidx";
        CREATE INDEX "jylland_7300_vejle_fjord_ler_rastidx" ON mstfohm."jylland_7300_vejle_fjord_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7300_vejle_fjord_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_7300_vejle_fjord_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7300_vejle_fjord_ler'::name,'rast'::name);
        
        /*
        * table:jylland_7400_billund_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_7400_billund_sand";
        
        CREATE TABLE mstfohm."jylland_7400_billund_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 7400
            )
        ;
        
        ALTER TABLE mstfohm."jylland_7400_billund_sand" DROP CONSTRAINT IF EXISTS "jylland_7400_billund_sand_pkey";
        ALTER TABLE mstfohm."jylland_7400_billund_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7400_billund_sand_rastidx";
        CREATE INDEX "jylland_7400_billund_sand_rastidx" ON mstfohm."jylland_7400_billund_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7400_billund_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_7400_billund_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7400_billund_sand'::name,'rast'::name);
        
        /*
        * table:jylland_7500_vejle_fjord_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_7500_vejle_fjord_ler";
        
        CREATE TABLE mstfohm."jylland_7500_vejle_fjord_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 7500
            )
        ;
        
        ALTER TABLE mstfohm."jylland_7500_vejle_fjord_ler" DROP CONSTRAINT IF EXISTS "jylland_7500_vejle_fjord_ler_pkey";
        ALTER TABLE mstfohm."jylland_7500_vejle_fjord_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7500_vejle_fjord_ler_rastidx";
        CREATE INDEX "jylland_7500_vejle_fjord_ler_rastidx" ON mstfohm."jylland_7500_vejle_fjord_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7500_vejle_fjord_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_7500_vejle_fjord_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7500_vejle_fjord_ler'::name,'rast'::name);
        
        /*
        * table:jylland_7600_billund_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_7600_billund_sand";
        
        CREATE TABLE mstfohm."jylland_7600_billund_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 7600
            )
        ;
        
        ALTER TABLE mstfohm."jylland_7600_billund_sand" DROP CONSTRAINT IF EXISTS "jylland_7600_billund_sand_pkey";
        ALTER TABLE mstfohm."jylland_7600_billund_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7600_billund_sand_rastidx";
        CREATE INDEX "jylland_7600_billund_sand_rastidx" ON mstfohm."jylland_7600_billund_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7600_billund_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_7600_billund_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7600_billund_sand'::name,'rast'::name);
        
        /*
        * table:jylland_7700_vejle_fjord_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_7700_vejle_fjord_ler";
        
        CREATE TABLE mstfohm."jylland_7700_vejle_fjord_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 7700
            )
        ;
        
        ALTER TABLE mstfohm."jylland_7700_vejle_fjord_ler" DROP CONSTRAINT IF EXISTS "jylland_7700_vejle_fjord_ler_pkey";
        ALTER TABLE mstfohm."jylland_7700_vejle_fjord_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7700_vejle_fjord_ler_rastidx";
        CREATE INDEX "jylland_7700_vejle_fjord_ler_rastidx" ON mstfohm."jylland_7700_vejle_fjord_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7700_vejle_fjord_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_7700_vejle_fjord_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7700_vejle_fjord_ler'::name,'rast'::name);
        
        /*
        * table:jylland_7800_billund_sand
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_7800_billund_sand";
        
        CREATE TABLE mstfohm."jylland_7800_billund_sand" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 7800
            )
        ;
        
        ALTER TABLE mstfohm."jylland_7800_billund_sand" DROP CONSTRAINT IF EXISTS "jylland_7800_billund_sand_pkey";
        ALTER TABLE mstfohm."jylland_7800_billund_sand" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7800_billund_sand_rastidx";
        CREATE INDEX "jylland_7800_billund_sand_rastidx" ON mstfohm."jylland_7800_billund_sand" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7800_billund_sand" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_7800_billund_sand from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7800_billund_sand'::name,'rast'::name);
        
        /*
        * table:jylland_8000_palaeogen_ler
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_8000_palaeogen_ler";
        
        CREATE TABLE mstfohm."jylland_8000_palaeogen_ler" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 8000
            )
        ;
        
        ALTER TABLE mstfohm."jylland_8000_palaeogen_ler" DROP CONSTRAINT IF EXISTS "jylland_8000_palaeogen_ler_pkey";
        ALTER TABLE mstfohm."jylland_8000_palaeogen_ler" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_8000_palaeogen_ler_rastidx";
        CREATE INDEX "jylland_8000_palaeogen_ler_rastidx" ON mstfohm."jylland_8000_palaeogen_ler" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_8000_palaeogen_ler" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_8000_palaeogen_ler from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_8000_palaeogen_ler'::name,'rast'::name);
        
        /*
        * table:jylland_8500_danien_kalk
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_8500_danien_kalk";
        
        CREATE TABLE mstfohm."jylland_8500_danien_kalk" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 8500
            )
        ;
        
        ALTER TABLE mstfohm."jylland_8500_danien_kalk" DROP CONSTRAINT IF EXISTS "jylland_8500_danien_kalk_pkey";
        ALTER TABLE mstfohm."jylland_8500_danien_kalk" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_8500_danien_kalk_rastidx";
        CREATE INDEX "jylland_8500_danien_kalk_rastidx" ON mstfohm."jylland_8500_danien_kalk" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_8500_danien_kalk" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_8500_danien_kalk from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_8500_danien_kalk'::name,'rast'::name);
        
        /*
        * table:jylland_9000_skrivekridt
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_9000_skrivekridt";
        
        CREATE TABLE mstfohm."jylland_9000_skrivekridt" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 9000
            )
        ;
        
        ALTER TABLE mstfohm."jylland_9000_skrivekridt" DROP CONSTRAINT IF EXISTS "jylland_9000_skrivekridt_pkey";
        ALTER TABLE mstfohm."jylland_9000_skrivekridt" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_9000_skrivekridt_rastidx";
        CREATE INDEX "jylland_9000_skrivekridt_rastidx" ON mstfohm."jylland_9000_skrivekridt" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_9000_skrivekridt" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_9000_skrivekridt from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_9000_skrivekridt'::name,'rast'::name);
        
        /*
        * table:jylland_9500_stensalt
        */
       
        DROP TABLE IF EXISTS mstfohm."jylland_9500_stensalt";
        
        CREATE TABLE mstfohm."jylland_9500_stensalt" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'jylland'
                    AND layernumber = 9500
            )
        ;
        
        ALTER TABLE mstfohm."jylland_9500_stensalt" DROP CONSTRAINT IF EXISTS "jylland_9500_stensalt_pkey";
        ALTER TABLE mstfohm."jylland_9500_stensalt" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_9500_stensalt_rastidx";
        CREATE INDEX "jylland_9500_stensalt_rastidx" ON mstfohm."jylland_9500_stensalt" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_9500_stensalt" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid jylland_9500_stensalt from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_9500_stensalt'::name,'rast'::name);
        
    COMMIT;
    
    GRANT USAGE ON SCHEMA fohm TO grukosreader;
    GRANT SELECT ON ALL TABLES IN SCHEMA fohm TO grukosreader;
    
    GRANT USAGE ON SCHEMA mstfohm TO grukosreader;
    GRANT SELECT ON ALL TABLES IN SCHEMA mstfohm TO grukosreader;
    
    