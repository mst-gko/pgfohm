import os
import time
import configparser
import subprocess
import glob
import re

import gdal
import numpy as np
import psycopg2 as pg
from sqlalchemy import create_engine
import rasterio  # use 'pip install rasterio', conda version creates dll error when imported per 20230116 on py3.9
import pandas as pd


class Database:

    def __init__(self, database, user):
        self.ini_dir = f'F:/GKO/data/grukos/db_credentials/{user.lower()}/{user.lower()}.ini'
        self.ini_section = database.upper()

    def connect_to_pg_db(self):
        db_connect_kwargs = self.parse_db_credentials()
        try:
            pg_con = pg.connect(**db_connect_kwargs)

        except Exception as e:
            print('Unable to connect to database: ' + db_connect_kwargs['dbname'])
            print(e)
        else:
            if db_connect_kwargs:
                del db_connect_kwargs

            if pg_con:
                return pg_con

    def execute_pg_sql(self, sql):
        with self.connect_to_pg_db() as con:
            with con.cursor() as cur:
                cur.execute(sql)
                con.commit()

    def parse_db_credentials(self):
        config = configparser.ConfigParser()
        config.read(self.ini_dir)

        db_connect_kwargs = {
            'dbname': f"{config[f'{self.ini_section}']['databasename']}",
            'user': f"{config[f'{self.ini_section}']['userid']}",
            'password': f"{config[f'{self.ini_section}']['password']}",
            'host': f"{config[f'{self.ini_section}']['host']}",
            'port': f"{config[f'{self.ini_section}']['port']}"
        }

        return db_connect_kwargs

    def create_engine(self):
        db_connect_kwargs = self.parse_db_credentials()
        pg_engine = create_engine(
            f"postgresql://{db_connect_kwargs['user']}:{db_connect_kwargs['password']}"
            f"@{db_connect_kwargs['host']}:{db_connect_kwargs['port']}/{db_connect_kwargs['dbname']}"
        )

        if db_connect_kwargs:
            del db_connect_kwargs

        return pg_engine

    def df_to_pgdb(self, df, table_name, schema_name, if_exists):
        with self.create_engine() as upload_engine:
            df.to_sql(
                name=table_name,
                con=upload_engine, schema=schema_name,
                if_exists=if_exists, index=False,
            )

    def postgis_raster_to_numpy(self, schema, table, join='', where_clause=''):
        sql = f'''
            SET postgis.gdal_enabled_drivers TO 'GTiff';

            SELECT 
                ST_AsGDALRaster(st_union(ST_Clip(rast, geom)), 'GTiff') 
            FROM {schema}."{table}"
            {join}
            {where_clause}
            ;
        '''

        con = self.connect_to_pg_db()
        cur = con.cursor()
        cur.execute(sql)

        for row in cur:
            rast = row[0].tobytes()
            with rasterio.io.MemoryFile(rast) as dataset:
                raster_arr = dataset.open().read()
                meta = dataset.open().meta
        return raster_arr, meta

    def postgis_raster_to_tiff_file(self, schema, table, path_output):

        sql = f"""
            SELECT 
                ST_AsGDALRaster(rast, 'GTiff', ARRAY['COMPRESS=LZW']) as tiff 
            FROM {schema}.{table} 
        """
        # db = Database(user='writer', database='FOHM')
        con = self.connect_to_pg_db()
        cur = con.cursor()
        cur.execute(sql)
        data = cur.fetchall()
        tif_data = data[0][0]  # This can passed as the data variable in the requests.put function
        with open(f"{path_output}/{schema}_{table}.tif", "wb") as f:
            f.write(tif_data)

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a postgres server database and store the output in a pandas dataframe
        :param sql: sql query for the import
        :return: pandas dataframe containing output from sql query
        """
        df1 = None
        with self.connect_to_pg_db() as con:
            try:
                df_pgsql = pd.read_sql(sql, con)
            except Exception as e:
                print(f'\n{e}')
            else:
                if not df_pgsql.empty:
                    return df_pgsql

    def fetch_tables_within_schema(self, schema):
        sql = f'''
            SELECT table_name
            FROM information_schema.tables
            WHERE table_schema='{schema}'
                AND table_type='BASE TABLE'
                AND table_name ILIKE 'jylland_%'
            ORDER BY table_name
        '''
        df_table_names = self.import_pgsql_to_df(sql)

        return df_table_names


class Raster:
    def __init__(self):
        pass

    @staticmethod
    def fetch_bounds_from_postgis_raster(schema, table):
        sql_extent = f'''
            SELECT 
                st_xmin(ST_Extent(ST_Envelope(rast))) AS x_min,
                st_xmax(ST_Extent(ST_Envelope(rast))) AS x_max,
                st_ymin(ST_Extent(ST_Envelope(rast))) AS y_min,
                st_ymax(ST_Extent(ST_Envelope(rast))) AS y_max
            FROM {schema}."{table}" 
        '''
        db = Database('fohm', 'writer')
        with db.connect_to_pg_db() as con:
            with con.cursor() as cur:
                cur.execute(sql_extent)
                result = cur.fetchone()
        print(result)
        left = int(result[0])
        right = int(result[1])
        bottom = int(result[2])
        top = int(result[3])

        return left, right, bottom, top

    @staticmethod
    def numpy_to_raster_file(raster_array, dst_path, meta, filetype='tif'):
        """
        On fohm layers (20220630) this functions displays an error on some of the cells, when using srid 25832
        It is not known why, but output file is as expected.
        :param dst_path: output path
        :param meta: meta data from rasterio raster
        :param filetype: output file type, supported 'grd' and 'tif'
        :param raster_array: numpy array
        """
        if filetype == 'tif':
            driver = 'GTiff'
        elif filetype == 'grd':
            driver = 'GS7BG'
        else:
            filetype = 'tif'
            driver = 'GTiff'

        dst_path_full = f'{dst_path}.{filetype}'

        with rasterio.open(
                dst_path_full,
                'w',
                driver=driver,
                height=raster_array.shape[1],
                width=raster_array.shape[2],
                count=1,
                dtype=str(raster_array.dtype),
                crs='EPSG:25832',
                transform=meta['transform']
        ) as rast:
            rast.write(raster_array)

    @staticmethod
    def create_empty_output_arr(template_array, fill_value=np.NaN):
        output_array = np.empty_like(template_array)
        output_array.fill(fill_value)

        return output_array

    @staticmethod
    def crop_raster_by_extent(raster, raster_extent):
        raster_to_be_clipped = gdal.Open(raster)
        raster_cropped = gdal.Translate('', raster_to_be_clipped, projWin=raster_extent, format='MEM')

        return raster_cropped

    @staticmethod
    def gdal_raster_to_numpy_array(gdal_rast, rasterband=1):
        array_rast = np.array(gdal_rast.GetRasterBand(rasterband).ReadAsArray())

        array_rast_arr = array_rast[np.newaxis, ...]
        np.insert(array_rast_arr, 0, rasterband, axis=0)

        return array_rast_arr

    @staticmethod
    def rasterio_to_pandas(src):
        xmin, ymax = np.around(src.xy(0.00, 0.00), 9)  # src.xy(0, 0)
        xmax, ymin = np.around(src.xy(src.height - 1, src.width - 1), 9)  # src.xy(src.width-1, src.height-1)
        x = np.linspace(xmin, xmax, src.width)
        y = np.linspace(ymax, ymin, src.height)  # max -> min so coords are top -> bottom

        # create 2D arrays
        xs, ys = np.meshgrid(x, y)
        zs = src.read(1)

        # Apply NoData mask
        mask = src.read_masks(1) > 0
        xs, ys, zs = xs[mask], ys[mask], zs[mask]

        data = {
            "X": pd.Series(xs.ravel()),
            "Y": pd.Series(ys.ravel()),
            "Z": pd.Series(zs.ravel())
        }

        df = pd.DataFrame(data=data)

        return df

    @staticmethod
    def export_raster_to_postgis(raster_path, database_name, schema_name, table_name):
        db = Database(database=database_name, user='writer')
        cred = db.parse_db_credentials()

        os.environ['PATH'] = r'C:\PostgreSQL\10\bin'
        os.environ['PGHOST'] = cred['host']
        os.environ['PGPORT'] = cred['port']
        os.environ['PGUSER'] = cred['user']
        os.environ['PGPASSWORD'] = cred['password']
        os.environ['PGDATABASE'] = cred['dbname']

        cmd_str = f"raster2pgsql.exe -I -C -s 25832 -t 200x200 {raster_path} {schema_name}.{table_name} | psql"

        subprocess.call(cmd_str, shell=True)

        del cred


def loop_raster_layers(database_name, schema_name, path_output):
    db = Database(database_name, 'writer')
    df_raster_names = db.fetch_tables_within_schema(schema=schema_name)
    r = Raster()

    arr_lst = []
    for index, row in df_raster_names.iterrows():
        print(f"loading layer {row['table_name']}...")
        join = f'''
            INNER JOIN 
                    (   
                        SELECT geom
                        FROM kort.kommunegraense k
                        WHERE kommunenr IN
                            (
                                657, 615, 707, 760, 741, 779, 
                                671, 810, 849, 820, 580, 540, 
                                573, 575, 730, 791, 530, 561, 
                                563, 607, 510, 621, 751, 710, 
                                766, 661, 756, 665, 727, 740, 
                                746, 706, 851, 813, 860, 846, 
                                773, 840, 787, 550, 630, 825 
                            )
                    ) AS k ON st_intersects(k.geom, rast)
        '''
        arr, meta = db.postgis_raster_to_numpy(schema=schema_name, table=row['table_name'], join=join)
        arr_lst.append((row['table_name'], arr, meta))

    arr_dem = arr_lst[0][1]
    dem_meta = arr_lst[0][2]

    depth_range = range(0, 301, 10)
    for k, depth in enumerate(depth_range):
        print(f'Processing depth:{depth}..')
        array_depth = r.create_empty_output_arr(template_array=arr_dem)

        for i2, arr in enumerate(arr_lst[1:]):
            raster_name = arr[0]
            arr_bot = arr[1]
            arr_top = arr_lst[i2 - 1][1]
            layer_bot_dep = arr_dem - arr_bot
            layer_top_dep = arr_dem - arr_top
            print(f'\t- processing layer:{raster_name}...')

            fohm_id = re.findall(r'\d+', raster_name)[0]
            arr_fohmid = r.create_empty_output_arr(fill_value=fohm_id, template_array=arr_dem)

            array_depth = np.where(
                np.isnan(array_depth),
                np.where(
                    (depth <= layer_bot_dep) & (depth >= layer_top_dep) & (layer_top_dep != layer_bot_dep),
                    arr_fohmid,
                    array_depth
                ),
                array_depth
            )

        r.numpy_to_raster_file(
            raster_array=array_depth,
            dst_path=f'{path_output}/fohm_slice_depth_{str(depth).zfill(3)}',
            meta=dem_meta
        )

    elevation_range = range(170, -301, -10)
    for j, elevation in enumerate(elevation_range):
        print(f'\nProcessing elevation:{elevation}..')
        array_elev = r.create_empty_output_arr(template_array=arr_dem)

        for i, arr in enumerate(arr_lst[1:]):
            raster_name = arr[0]
            arr_bot = arr[1]
            arr_top = arr_lst[i - 1][1]
            print(f'\t- processing layer:{raster_name}...')

            fohm_id = re.findall(r'\d+', raster_name)[0]
            arr_fohmid = r.create_empty_output_arr(fill_value=fohm_id, template_array=arr_dem)
            array_elev = np.where(
                np.isnan(array_elev),
                np.where(
                    (elevation >= arr_bot) & (elevation <= arr_top) & (arr_top != arr_bot),
                    arr_fohmid,
                    array_elev
                ),
                array_elev
            )

        path_orig = f'{path_output}/fohm_slice_elevation_{str(elevation).replace("-", "neg")}'
        num_from_string = int(re.findall('\d+', path_orig)[0])
        dst_path = re.sub('\d+', '{:03}'.format(num_from_string), path_orig)
        r.numpy_to_raster_file(
            raster_array=array_elev,
            dst_path=dst_path,
            meta=dem_meta
        )


def loop_tif_files(path_input, database_name, schema_name):
    tif_paths = glob.glob(f'{path_input}/*.tif')
    r = Raster()

    db = Database(database_name, 'writer')
    db.execute_pg_sql(f'CREATE SCHEMA IF NOT EXISTS {schema_name};')

    for tif_path in tif_paths:
        base = os.path.basename(tif_path)
        table_name = os.path.splitext(base)[0]

        r.export_raster_to_postgis(
            raster_path=tif_path,
            database_name=database_name,
            schema_name=schema_name,
            table_name=table_name
        )

    sql_permit = f'''
        GRANT USAGE ON SCHEMA {schema_name} TO grukosreader;
        GRANT SELECT ON ALL TABLES IN SCHEMA {schema_name} TO grukosreader;
    '''
    db.execute_pg_sql(sql_permit)


# start timer
t = time.time()
database_name = 'grukos'
schema_name_input = 'mstfohm'
schema_name_output = 'mstfohm'
path_tif_files = './'

loop_raster_layers(database_name=database_name, schema_name=schema_name_input, path_output=path_tif_files)
loop_tif_files(database_name=database_name, schema_name=schema_name_output, path_input=path_tif_files)

elapsed = round(time.time() - t, 2)
print(f'\n{os.path.basename(__file__)} executed in {elapsed} s')
