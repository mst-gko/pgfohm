@echo off 

REM set vars
SET PGCLIENTENCODING=UTF8
SET PGPASSWORD=%DB_ADMIN_PASS%
SET pg_hostname=10.33.131.50
SET pg_username=%DB_ADMIN_USER%
SET pg_port=5432
SET dbname=fohm
SET schema_name=fohm_layer
SET path_files=%~dp0

echo running fohm_flader_boreholes.py
python %~dp0\fohm_flader_boreholes.py 

cd C:\PostgreSQL\10\bin
psql -h %pg_hostname% -p %pg_port% -U %pg_username% -d %dbname% -f %path_files%\fohm_filter_boreholes.sql
