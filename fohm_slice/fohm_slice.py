import os
import configparser
import subprocess
import glob
import re
import shutil

import numpy as np
from geotiff import GeoTiff
import tifffile as tf
import psycopg2 as pg
from sqlalchemy import create_engine
import pandas as pd

exit()

# TODO rewrite loop_tiff_files / loop_elevation_and_depth so tiff files are only loaded once


class Database:

    def __init__(self, database, user):
        self.ini_dir = f'F:/GKO/data/grukos/db_credentials/{user.lower()}/{user.lower()}.ini'
        self.ini_section = database.upper()

    def connect_to_pg_db(self):
        db_connect_kwargs = self.parse_db_credentials()
        try:
            pg_con = pg.connect(**db_connect_kwargs)

        except Exception as e:
            print('Unable to connect to database: ' + db_connect_kwargs['dbname'])
            print(e)
        else:
            if db_connect_kwargs:
                del db_connect_kwargs

            if pg_con:
                return pg_con

    def execute_pg_sql(self, sql):
        with self.connect_to_pg_db() as con:
            with con.cursor() as cur:
                cur.execute(sql)
                con.commit()

    def parse_db_credentials(self):
        config = configparser.ConfigParser()
        config.read(self.ini_dir)

        db_connect_kwargs = {
            'dbname': f"{config[f'{self.ini_section}']['databasename']}",
            'user': f"{config[f'{self.ini_section}']['userid']}",
            'password': f"{config[f'{self.ini_section}']['password']}",
            'host': f"{config[f'{self.ini_section}']['host']}",
            'port': f"{config[f'{self.ini_section}']['port']}"
        }

        return db_connect_kwargs

    def create_engine(self):
        db_connect_kwargs = self.parse_db_credentials()
        pg_engine = create_engine(
            f"postgresql://{db_connect_kwargs['user']}:{db_connect_kwargs['password']}"
            f"@{db_connect_kwargs['host']}:{db_connect_kwargs['port']}/{db_connect_kwargs['dbname']}"
        )

        if db_connect_kwargs:
            del db_connect_kwargs

        return pg_engine

    def df_to_pgdb(self, df, table_name, schema_name, if_exists):
        with self.create_engine() as upload_engine:
            df.to_sql(
                name=table_name,
                con=upload_engine, schema=schema_name,
                if_exists=if_exists, index=False,
            )

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a postgres server database and store the output in a pandas dataframe
        :param sql: sql query for the import
        :return: pandas dataframe containing output from sql query
        """
        with self.connect_to_pg_db() as con:
            try:
                df_pgsql = pd.read_sql(sql, con)
            except Exception as e:
                print(f'\n{e}')
            else:
                if not df_pgsql.empty:
                    return df_pgsql


class GeoTiffFile:
    """
    No gdal handling of local tiff files
    """
    def __init__(self, geotiff_path):
        self.geotiff_path = geotiff_path

    def open_geotiff_file(self, srid=25832):
        geo_tiff = GeoTiff(self.geotiff_path, crs_code=srid)

        return geo_tiff

    def geotiff_file_to_zarr_array(self):
        geo_tiff = self.open_geotiff_file()
        zarr_array = geo_tiff.read()

        return zarr_array

    def geotiff_file_to_numpy(self):
        raster_zarr_array = self.geotiff_file_to_zarr_array
        raster_np_array = np.array(raster_zarr_array())

        return raster_np_array


class PostgisRaster:
    """
    No gdal handling of postgis raster layers
    """
    def __init__(self, database, schema, table):
        self.database = database
        self.schema = schema
        self.table = table

    def fetch_raster_georefcoords(self):
        sql = f'''
        WITH 
            tmp1 AS
                (
                    SELECT ST_MetaData(st_union(rast)) AS meta 
                    FROM {self.schema}.{self.table} jks 
                ),
            tmp2 AS 
                (
                    SELECT 
                        (meta).scalex,
                        (meta).skewy, 
                        (meta).skewx,
                        (meta).scaley,
                        (meta).upperleftx,
                        (meta).upperlefty
                    FROM tmp1
                )
        SELECT 
            scalex || ' ' || skewy || ' ' || skewx  || ' ' || scaley  || ' ' || upperleftx  || ' ' || upperlefty AS georefcoords
        FROM tmp2
        '''

        db = Database(user='writer', database='GRUKOS')
        con = db.connect_to_pg_db()
        cur = con.cursor()
        cur.execute(sql)
        data = cur.fetchall()
        georef_str = data[0][0]
        del cur, con, db

        return georef_str

    def import_postgis_raster_to_tiff_file(self, outfile_path=None, sql_join='', where_clause=''):
        sql = f"""
            SET postgis.gdal_enabled_drivers TO 'GTiff';
        
            SELECT 
                ST_AsGDALRaster(ST_Union(rast), 'GTiff', ARRAY['COMPRESS=LZW']) as tiff 
            FROM {self.schema}.{self.table} {sql_join} {where_clause};
        """

        db = Database(user='writer', database=self.database)
        con = db.connect_to_pg_db()
        cur = con.cursor()
        cur.execute(sql)
        data = cur.fetchall()
        tif_data = data[0][0]  # This can passed as the data variable in the requests.put function
        del cur, con, db

        if not outfile_path:
            outfile_path = f"{self.table}.tif"

        with open(outfile_path, "wb") as f:
            f.write(tif_data)

    @staticmethod
    def export_numpy_to_tiff_file(raster_numpy_array, outfile_path=None):
        if not outfile_path:
            outfile_path = f"numpy_to_tiff_file.tif"

        tf_writer = tf.TiffWriter(outfile_path)
        tf_writer.write(data=raster_numpy_array)

        return outfile_path

    def export_raster_file_to_postgis(self, raster_path, georefcoords_string, postgis_path=r'C:\PostgreSQL\10\bin'):
        # dependencies to postgres with postgis
        db = Database(database=self.database, user='writer')
        cred = db.parse_db_credentials()

        os.environ['PATH'] = postgis_path
        os.environ['PGHOST'] = cred['host']
        os.environ['PGPORT'] = cred['port']
        os.environ['PGUSER'] = cred['user']
        os.environ['PGPASSWORD'] = cred['password']
        os.environ['PGDATABASE'] = cred['dbname']

        cmd_str = f"raster2pgsql.exe -I -C -d -s 25832 {raster_path} {self.schema}.{self.table} | psql"

        subprocess.call(cmd_str, shell=True)
        del db, cred

        self.set_georefence_on_postgis_raster(georefcoords_string=georefcoords_string)

    def set_georefence_on_postgis_raster(self, georefcoords_string='100 0 0 -100 438450 6409950'):

        sql = f'''
        CREATE TABLE {self.schema}.{self.table}_2 AS
            (
                SELECT 
                    1 AS rid,
                    ST_SetGeoReference(rast, '{georefcoords_string}', 'GDAL') AS rast
                FROM {self.schema}.{self.table}
            )
        ;
        
        DROP TABLE IF EXISTS {self.schema}.{self.table};
        
        ALTER TABLE {self.schema}.{self.table}_2 RENAME TO {self.table};
        '''

        db = Database(database='grukos', user='writer')
        db.execute_pg_sql(sql=sql)

        del db


def fetch_fohm_layers_jylland(database, schema):
    sql = f'''
        SELECT table_name
        FROM information_schema.tables
        WHERE table_schema='{schema}'
            AND table_type='BASE TABLE'
            AND table_name ILIKE 'jylland_%'
        ORDER BY table_name
    '''

    db = Database(database=database, user='writer')
    df_table_names = db.import_pgsql_to_df(sql)

    return df_table_names


def create_numpy_array_from_template(template_array, fill_value=np.NaN):
    output_array = np.empty_like(template_array)
    output_array.fill(fill_value)

    return output_array


def fohm_layers_to_tiff_files(root_path):
    database = 'GRUKOS'
    schema = 'mstfohm'
    df_fohm_layer_names = fetch_fohm_layers_jylland(database=database, schema=schema)

    # delete tiff subfolder with content and create subfolder
    if os.path.exists(root_path):
        shutil.rmtree(root_path)
    os.makedirs(root_path)

    for index, row in df_fohm_layer_names.iterrows():
        table = row['table_name']
        tiff_file_name = f'{root_path}/{table}.tif'
        print(f'Saving {table} as a tiff file {tiff_file_name}...')
        fohm_raster = PostgisRaster(database, schema, table)
        sql_join_string = f'''
        INNER JOIN 
            (   
                SELECT geom
                FROM kort.kommunegraense k
                WHERE kommunenr IN
                    (
                        657, 615, 707, 760, 741, 779, 
                        671, 810, 849, 820, 580, 540, 
                        573, 575, 730, 791, 530, 561, 
                        563, 607, 510, 621, 751, 710, 
                        766, 661, 756, 665, 727, 740, 
                        746, 706, 851, 813, 860, 846, 
                        773, 840, 787, 550, 630, 825 
                    )
            ) AS k ON st_intersects(k.geom, rast)
        '''
        fohm_raster.import_postgis_raster_to_tiff_file(outfile_path=tiff_file_name, sql_join=sql_join_string)


def loop_tiff_files(path_input, depth, elevation):

    tiff_paths = glob.glob(f'{path_input}/*.tif')

    dem_path = tiff_paths[0]
    dem_tiff = GeoTiffFile(dem_path)
    arr_dem = dem_tiff.geotiff_file_to_numpy()
    array_slice_elevation = create_numpy_array_from_template(template_array=arr_dem)
    array_slice_depth = create_numpy_array_from_template(template_array=arr_dem)

    for i, tiff_path in enumerate(tiff_paths):
        print(f'    - tiff file {tiff_path}')
        base = os.path.basename(tiff_path)
        filename = os.path.splitext(base)[0]
        match_fohm_id = re.findall('\d+', filename)
        fohm_id = str(match_fohm_id[0])

        if fohm_id == '0000':
            continue

        arr_fohmid = create_numpy_array_from_template(fill_value=fohm_id, template_array=arr_dem)

        tiff_bot = GeoTiffFile(tiff_path)
        arr_bot = tiff_bot.geotiff_file_to_numpy()

        path_tiff_top = tiff_paths[i - 1]
        tiff_top = GeoTiffFile(path_tiff_top)
        arr_top = tiff_top.geotiff_file_to_numpy()

        arr_bot_dep = arr_dem - arr_bot
        arr_top_dep = arr_dem - arr_top

        array_slice_elevation = np.where(
            np.isnan(array_slice_elevation),
            np.where(
                (elevation >= arr_bot) & (elevation <= arr_top) & (arr_top != arr_bot),
                arr_fohmid,
                array_slice_elevation
            ),
            array_slice_elevation
        )

        array_slice_depth = np.where(
            np.isnan(array_slice_depth),
            np.where(
                (depth <= arr_bot_dep) & (depth >= arr_top_dep) & (arr_top_dep != arr_bot_dep),
                arr_fohmid,
                array_slice_depth
            ),
            array_slice_depth
        )

    return array_slice_depth, array_slice_elevation


def loop_elevation_and_depth(root_path, depth_list, elevation_list, georef_string):

    for elevation_value, depth_value in zip(elevation_list, depth_list):
        print(f'Depth:{depth_value}, Elevation:{elevation_value}...')

        # loop through all tiff files containing fohm layers
        array_slice_depth, array_slice_elevation = loop_tiff_files(path_input=root_path, depth=depth_value, elevation=elevation_value)

        # upload depth slice to grukos
        outfile_slice_depth = './temp_slice_depth.tif'
        pgr = PostgisRaster(database='GRUKOS', schema='mstfohm', table=f'jylland_slice_dybde_{str(depth_value).zfill(3)}')
        pgr.export_numpy_to_tiff_file(raster_numpy_array=array_slice_depth, outfile_path=outfile_slice_depth)
        pgr.export_raster_file_to_postgis(raster_path=outfile_slice_depth, georefcoords_string=georef_string)

        # upload elevation slice to grukos
        outfile_slice_ele = './temp_slice_ele.tif'
        pgr_ele = PostgisRaster(database='GRUKOS', schema='mstfohm', table=f'jylland_slice_elevation_{str(elevation_value).replace("-", "neg")}')
        pgr_ele.export_numpy_to_tiff_file(raster_numpy_array=array_slice_elevation, outfile_path=outfile_slice_ele)
        pgr_ele.export_raster_file_to_postgis(raster_path=outfile_slice_ele, georefcoords_string=georef_string)

        # cleanup the temporary tiff files containing the slice
        try:
            os.remove(outfile_slice_depth)
        except OSError:
            pass

        try:
            os.remove(outfile_slice_ele)
        except OSError:
            pass


# path where tiff files are stored e.g. c:/folder/subfolder
root_path = 'c:/gitlab/fohm_slice/fohm_tiffs'

# depth and elevation lists to loop through
# best if the ele and dep lists are equal lengths
depth_list = range(0, 370, 10)
elevation_list = range(160, -210, -10)

# fetch georefstring for raster import to postgis
# for jutland it is most likely: georef_string = '100 0 0 -100 438450 6409950'
pgr = PostgisRaster(database='GRUKOS', schema='mstfohm', table='jylland_0000_terraen')
georef_string = pgr.fetch_raster_georefcoords()

# save fohm layers from grukos to temporary store local tiff files
fohm_layers_to_tiff_files(root_path)

# loop through all tiff files for each depth and elevation
loop_elevation_and_depth(root_path=root_path, depth_list=depth_list, elevation_list=elevation_list, georef_string=georef_string)

# clean up temporary subfolder containing tiff files
if os.path.exists(root_path):
    shutil.rmtree(root_path)
