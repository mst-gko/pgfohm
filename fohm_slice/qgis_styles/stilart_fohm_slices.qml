<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+08" version="3.22.0-Białowieża" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal fetchMode="0" enabled="0" mode="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <Option type="Map">
      <Option type="bool" value="false" name="WMSBackgroundLayer"/>
      <Option type="bool" value="false" name="WMSPublishDataSourceUrl"/>
      <Option type="int" value="0" name="embeddedWidgets/count"/>
      <Option type="QString" value="Value" name="identify/format"/>
    </Option>
  </customproperties>
  <pipe-data-defined-properties>
    <Option type="Map">
      <Option type="QString" value="" name="name"/>
      <Option name="properties"/>
      <Option type="QString" value="collection" name="type"/>
    </Option>
  </pipe-data-defined-properties>
  <pipe>
    <provider>
      <resampling zoomedInResamplingMethod="nearestNeighbour" enabled="false" zoomedOutResamplingMethod="nearestNeighbour" maxOversampling="2"/>
    </provider>
    <rasterrenderer nodataColor="" type="singlebandpseudocolor" opacity="1" band="1" classificationMax="9000" alphaBand="-1" classificationMin="50">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader classificationMode="1" labelPrecision="4" clip="0" maximumValue="9000" minimumValue="50" colorRampType="INTERPOLATED">
          <colorramp type="gradient" name="[source]">
            <Option type="Map">
              <Option type="QString" value="255,0,191,255" name="color1"/>
              <Option type="QString" value="124,252,0,255" name="color2"/>
              <Option type="QString" value="0" name="discrete"/>
              <Option type="QString" value="gradient" name="rampType"/>
              <Option type="QString" value="0.00558659;0,190,35,255:0.0167598;255,0,0,255:0.027933;160,82,45,255:0.0391061;255,0,0,255:0.117318;160,82,45,255:0.128492;255,0,0,255:0.139665;160,82,45,255:0.150838;255,0,0,255:0.162011;160,82,45,255:0.22905;255,0,0,255:0.240223;160,82,45,255:0.251397;255,0,0,255:0.26257;160,82,45,255:0.564246;255,140,0,255:0.575419;0,191,255,255:0.586592;255,140,0,255:0.597765;0,191,255,255:0.608939;255,140,0,255:0.620112;0,191,255,255:0.631285;255,140,0,255:0.642458;0,191,255,255:0.653631;255,140,0,255:0.664804;0,191,255,255:0.675978;255,140,0,255:0.687151;0,191,255,255:0.698324;255,140,0,255:0.709497;0,191,255,255:0.72067;255,140,0,255:0.731844;0,191,255,255:0.743017;255,140,0,255:0.75419;0,191,255,255:0.765363;255,140,0,255:0.776536;0,191,255,255:0.787709;255,140,0,255:0.798883;0,191,255,255:0.810056;255,140,0,255:0.821229;0,191,255,255:0.832402;255,140,0,255:0.843575;0,191,255,255:0.854749;255,140,0,255:0.865922;0,191,255,255:0.888268;0,0,128,255:0.944134;0,100,0,255" name="stops"/>
            </Option>
            <prop v="255,0,191,255" k="color1"/>
            <prop v="124,252,0,255" k="color2"/>
            <prop v="0" k="discrete"/>
            <prop v="gradient" k="rampType"/>
            <prop v="0.00558659;0,190,35,255:0.0167598;255,0,0,255:0.027933;160,82,45,255:0.0391061;255,0,0,255:0.117318;160,82,45,255:0.128492;255,0,0,255:0.139665;160,82,45,255:0.150838;255,0,0,255:0.162011;160,82,45,255:0.22905;255,0,0,255:0.240223;160,82,45,255:0.251397;255,0,0,255:0.26257;160,82,45,255:0.564246;255,140,0,255:0.575419;0,191,255,255:0.586592;255,140,0,255:0.597765;0,191,255,255:0.608939;255,140,0,255:0.620112;0,191,255,255:0.631285;255,140,0,255:0.642458;0,191,255,255:0.653631;255,140,0,255:0.664804;0,191,255,255:0.675978;255,140,0,255:0.687151;0,191,255,255:0.698324;255,140,0,255:0.709497;0,191,255,255:0.72067;255,140,0,255:0.731844;0,191,255,255:0.743017;255,140,0,255:0.75419;0,191,255,255:0.765363;255,140,0,255:0.776536;0,191,255,255:0.787709;255,140,0,255:0.798883;0,191,255,255:0.810056;255,140,0,255:0.821229;0,191,255,255:0.832402;255,140,0,255:0.843575;0,191,255,255:0.854749;255,140,0,255:0.865922;0,191,255,255:0.888268;0,0,128,255:0.944134;0,100,0,255" k="stops"/>
          </colorramp>
          <item value="50" label="50,0000" alpha="255" color="#ff00bf"/>
          <item value="100" label="100,0000" alpha="255" color="#00be23"/>
          <item value="200" label="200,0000" alpha="255" color="#ff0000"/>
          <item value="300" label="300,0000" alpha="255" color="#a0522d"/>
          <item value="400" label="400,0000" alpha="255" color="#ff0000"/>
          <item value="1100" label="1100,0000" alpha="255" color="#a0522d"/>
          <item value="1200" label="1200,0000" alpha="255" color="#ff0000"/>
          <item value="1300" label="1300,0000" alpha="255" color="#a0522d"/>
          <item value="1400" label="1400,0000" alpha="255" color="#ff0000"/>
          <item value="1500" label="1500,0000" alpha="255" color="#a0522d"/>
          <item value="2100" label="2100,0000" alpha="255" color="#ff0000"/>
          <item value="2200" label="2200,0000" alpha="255" color="#a0522d"/>
          <item value="2300" label="2300,0000" alpha="255" color="#ff0000"/>
          <item value="2400" label="2400,0000" alpha="255" color="#a0522d"/>
          <item value="5100" label="5100,0000" alpha="255" color="#ff8c00"/>
          <item value="5200" label="5200,0000" alpha="255" color="#00bfff"/>
          <item value="5300" label="5300,0000" alpha="255" color="#ff8c00"/>
          <item value="5400" label="5400,0000" alpha="255" color="#00bfff"/>
          <item value="5500" label="5500,0000" alpha="255" color="#ff8c00"/>
          <item value="5600" label="5600,0000" alpha="255" color="#00bfff"/>
          <item value="5700" label="5700,0000" alpha="255" color="#ff8c00"/>
          <item value="5800" label="5800,0000" alpha="255" color="#00bfff"/>
          <item value="5900" label="5900,0000" alpha="255" color="#ff8c00"/>
          <item value="6000" label="6000,0000" alpha="255" color="#00bfff"/>
          <item value="6100" label="6100,0000" alpha="255" color="#ff8c00"/>
          <item value="6200" label="6200,0000" alpha="255" color="#00bfff"/>
          <item value="6300" label="6300,0000" alpha="255" color="#ff8c00"/>
          <item value="6400" label="6400,0000" alpha="255" color="#00bfff"/>
          <item value="6500" label="6500,0000" alpha="255" color="#ff8c00"/>
          <item value="6600" label="6600,0000" alpha="255" color="#00bfff"/>
          <item value="6700" label="6700,0000" alpha="255" color="#ff8c00"/>
          <item value="6800" label="6800,0000" alpha="255" color="#00bfff"/>
          <item value="6900" label="6900,0000" alpha="255" color="#ff8c00"/>
          <item value="7000" label="7000,0000" alpha="255" color="#00bfff"/>
          <item value="7100" label="7100,0000" alpha="255" color="#ff8c00"/>
          <item value="7200" label="7200,0000" alpha="255" color="#00bfff"/>
          <item value="7300" label="7300,0000" alpha="255" color="#ff8c00"/>
          <item value="7400" label="7400,0000" alpha="255" color="#00bfff"/>
          <item value="7500" label="7500,0000" alpha="255" color="#ff8c00"/>
          <item value="7600" label="7600,0000" alpha="255" color="#00bfff"/>
          <item value="7700" label="7700,0000" alpha="255" color="#ff8c00"/>
          <item value="7800" label="7800,0000" alpha="255" color="#00bfff"/>
          <item value="8000" label="8000,0000" alpha="255" color="#000080"/>
          <item value="8500" label="8500,0000" alpha="255" color="#006400"/>
          <item value="9000" label="9000,0000" alpha="255" color="#7cfc00"/>
          <rampLegendSettings direction="0" orientation="2" minimumLabel="" maximumLabel="" prefix="" useContinuousLegend="1" suffix="">
            <numericFormat id="basic">
              <Option type="Map">
                <Option type="QChar" value="" name="decimal_separator"/>
                <Option type="int" value="6" name="decimals"/>
                <Option type="int" value="0" name="rounding_type"/>
                <Option type="bool" value="false" name="show_plus"/>
                <Option type="bool" value="true" name="show_thousand_separator"/>
                <Option type="bool" value="false" name="show_trailing_zeros"/>
                <Option type="QChar" value="" name="thousand_separator"/>
              </Option>
            </numericFormat>
          </rampLegendSettings>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast gamma="1" contrast="0" brightness="0"/>
    <huesaturation colorizeOn="0" saturation="0" invertColors="0" grayscaleMode="0" colorizeStrength="100" colorizeBlue="128" colorizeGreen="128" colorizeRed="255"/>
    <rasterresampler maxOversampling="2"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
