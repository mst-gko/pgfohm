
    BEGIN;
        
        CREATE SCHEMA IF NOT EXISTS mstfohm;
        
        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON SCHEMA mstfohm IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: all grids from fohm'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
    
        /*
        * table:fyn_0001_topo
        */
       
        DROP TABLE IF EXISTS mstfohm."fyn_0001_topo";
        
        CREATE TABLE mstfohm."fyn_0001_topo" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'fyn'
                    AND layernumber = 21
            )
        ;
        
        ALTER TABLE mstfohm."fyn_0001_topo" DROP CONSTRAINT IF EXISTS "fyn_0001_topo_pkey";
        ALTER TABLE mstfohm."fyn_0001_topo" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0001_topo_rastidx";
        CREATE INDEX "fyn_0001_topo_rastidx" ON mstfohm."fyn_0001_topo" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0001_topo" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid fyn_0001_topo from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0001_topo'::name,'rast'::name);
        
        /*
        * table:fyn_0002_kl1
        */
       
        DROP TABLE IF EXISTS mstfohm."fyn_0002_kl1";
        
        CREATE TABLE mstfohm."fyn_0002_kl1" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'fyn'
                    AND layernumber = 22
            )
        ;
        
        ALTER TABLE mstfohm."fyn_0002_kl1" DROP CONSTRAINT IF EXISTS "fyn_0002_kl1_pkey";
        ALTER TABLE mstfohm."fyn_0002_kl1" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0002_kl1_rastidx";
        CREATE INDEX "fyn_0002_kl1_rastidx" ON mstfohm."fyn_0002_kl1" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0002_kl1" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid fyn_0002_kl1 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0002_kl1'::name,'rast'::name);
        
        /*
        * table:fyn_0003_ks1
        */
       
        DROP TABLE IF EXISTS mstfohm."fyn_0003_ks1";
        
        CREATE TABLE mstfohm."fyn_0003_ks1" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'fyn'
                    AND layernumber = 23
            )
        ;
        
        ALTER TABLE mstfohm."fyn_0003_ks1" DROP CONSTRAINT IF EXISTS "fyn_0003_ks1_pkey";
        ALTER TABLE mstfohm."fyn_0003_ks1" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0003_ks1_rastidx";
        CREATE INDEX "fyn_0003_ks1_rastidx" ON mstfohm."fyn_0003_ks1" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0003_ks1" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid fyn_0003_ks1 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0003_ks1'::name,'rast'::name);
        
        /*
        * table:fyn_0004_kl2
        */
       
        DROP TABLE IF EXISTS mstfohm."fyn_0004_kl2";
        
        CREATE TABLE mstfohm."fyn_0004_kl2" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'fyn'
                    AND layernumber = 24
            )
        ;
        
        ALTER TABLE mstfohm."fyn_0004_kl2" DROP CONSTRAINT IF EXISTS "fyn_0004_kl2_pkey";
        ALTER TABLE mstfohm."fyn_0004_kl2" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0004_kl2_rastidx";
        CREATE INDEX "fyn_0004_kl2_rastidx" ON mstfohm."fyn_0004_kl2" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0004_kl2" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid fyn_0004_kl2 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0004_kl2'::name,'rast'::name);
        
        /*
        * table:fyn_0005_ks2
        */
       
        DROP TABLE IF EXISTS mstfohm."fyn_0005_ks2";
        
        CREATE TABLE mstfohm."fyn_0005_ks2" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'fyn'
                    AND layernumber = 25
            )
        ;
        
        ALTER TABLE mstfohm."fyn_0005_ks2" DROP CONSTRAINT IF EXISTS "fyn_0005_ks2_pkey";
        ALTER TABLE mstfohm."fyn_0005_ks2" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0005_ks2_rastidx";
        CREATE INDEX "fyn_0005_ks2_rastidx" ON mstfohm."fyn_0005_ks2" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0005_ks2" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid fyn_0005_ks2 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0005_ks2'::name,'rast'::name);
        
        /*
        * table:fyn_0006_kl3
        */
       
        DROP TABLE IF EXISTS mstfohm."fyn_0006_kl3";
        
        CREATE TABLE mstfohm."fyn_0006_kl3" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'fyn'
                    AND layernumber = 26
            )
        ;
        
        ALTER TABLE mstfohm."fyn_0006_kl3" DROP CONSTRAINT IF EXISTS "fyn_0006_kl3_pkey";
        ALTER TABLE mstfohm."fyn_0006_kl3" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0006_kl3_rastidx";
        CREATE INDEX "fyn_0006_kl3_rastidx" ON mstfohm."fyn_0006_kl3" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0006_kl3" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid fyn_0006_kl3 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0006_kl3'::name,'rast'::name);
        
        /*
        * table:fyn_0007_ks3
        */
       
        DROP TABLE IF EXISTS mstfohm."fyn_0007_ks3";
        
        CREATE TABLE mstfohm."fyn_0007_ks3" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'fyn'
                    AND layernumber = 27
            )
        ;
        
        ALTER TABLE mstfohm."fyn_0007_ks3" DROP CONSTRAINT IF EXISTS "fyn_0007_ks3_pkey";
        ALTER TABLE mstfohm."fyn_0007_ks3" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0007_ks3_rastidx";
        CREATE INDEX "fyn_0007_ks3_rastidx" ON mstfohm."fyn_0007_ks3" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0007_ks3" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid fyn_0007_ks3 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0007_ks3'::name,'rast'::name);
        
        /*
        * table:fyn_0008_kl4
        */
       
        DROP TABLE IF EXISTS mstfohm."fyn_0008_kl4";
        
        CREATE TABLE mstfohm."fyn_0008_kl4" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'fyn'
                    AND layernumber = 28
            )
        ;
        
        ALTER TABLE mstfohm."fyn_0008_kl4" DROP CONSTRAINT IF EXISTS "fyn_0008_kl4_pkey";
        ALTER TABLE mstfohm."fyn_0008_kl4" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0008_kl4_rastidx";
        CREATE INDEX "fyn_0008_kl4_rastidx" ON mstfohm."fyn_0008_kl4" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0008_kl4" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid fyn_0008_kl4 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0008_kl4'::name,'rast'::name);
        
        /*
        * table:fyn_0009_pl1
        */
       
        DROP TABLE IF EXISTS mstfohm."fyn_0009_pl1";
        
        CREATE TABLE mstfohm."fyn_0009_pl1" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'fyn'
                    AND layernumber = 29
            )
        ;
        
        ALTER TABLE mstfohm."fyn_0009_pl1" DROP CONSTRAINT IF EXISTS "fyn_0009_pl1_pkey";
        ALTER TABLE mstfohm."fyn_0009_pl1" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0009_pl1_rastidx";
        CREATE INDEX "fyn_0009_pl1_rastidx" ON mstfohm."fyn_0009_pl1" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0009_pl1" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid fyn_0009_pl1 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0009_pl1'::name,'rast'::name);
        
        /*
        * table:fyn_0010_kalk
        */
       
        DROP TABLE IF EXISTS mstfohm."fyn_0010_kalk";
        
        CREATE TABLE mstfohm."fyn_0010_kalk" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'fyn'
                    AND layernumber = 30
            )
        ;
        
        ALTER TABLE mstfohm."fyn_0010_kalk" DROP CONSTRAINT IF EXISTS "fyn_0010_kalk_pkey";
        ALTER TABLE mstfohm."fyn_0010_kalk" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0010_kalk_rastidx";
        CREATE INDEX "fyn_0010_kalk_rastidx" ON mstfohm."fyn_0010_kalk" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0010_kalk" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid fyn_0010_kalk from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0010_kalk'::name,'rast'::name);
        
    COMMIT;
    
    GRANT USAGE ON SCHEMA fohm TO grukosreader;
    GRANT SELECT ON ALL TABLES IN SCHEMA fohm TO grukosreader;
    
    GRANT USAGE ON SCHEMA mstfohm TO grukosreader;
    GRANT SELECT ON ALL TABLES IN SCHEMA mstfohm TO grukosreader;
    
    