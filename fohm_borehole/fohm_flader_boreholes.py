import time
import os
import re
import configparser
import psycopg2 as pg
import pandas as pd

__author__ = 'Simon Makwarth <simak@mst.dk>'
__description__ = '''
Creates a table within MST-GKO fohm database containing the following:
    1) The elevation from each of the fohm layers at the x-y coord from all jupiter boreholes, 
        creating a table for each fohm layer.
    2) All elevation from fohm layers with non-zero thickness creating top and bottom of each layer 
        at the x-y coord from all jupiter boreholes.
    3) The fohm layer(s) covering the filter elevation of each borehole containing filter top and bottom in jupiter db
    4) Same as 3) but the aquifer convering the majority of the filter is prefered 
        if any aquiferlayer is covering the jupiter borehole filter.
'''
__license__ = 'GNU GPLv3'


class Database:
    """Class handles all database related function"""

    def __init__(self, database_name, usr):
        """
        Initiates the classe Database
        :param database_name: string used to specify which database name on MST-Grundvand server to connect to
        :param usr: string used to specify the user, e.g. reader og writer
        """
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    @property
    def parse_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: dict containing db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def connect_to_pgsql_db(self):
        """
        Connects to postgresql server database
        :return: postgresql psycopg2 connection and string database name
        """
        cred = self.parse_db_credentials
        pg_database_name = cred['dbname']
        pg_con = None
        try:
            pg_con = pg.connect(
                host=cred['host'],
                port=cred['port'],
                database=cred['dbname'],
                user=cred['usr'],
                password=cred['pw']
            )
        except Exception as e:
            print(e)
        else:
            del cred
            # print(f'Connected to database: {pg_database_name}')

        return pg_con, pg_database_name

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query string for the import
        :return: pandas dataframe containing output from sql query
        """
        conn, database = self.connect_to_pgsql_db()
        try:
            # print(f'Fetching table from {database} to dataframe...')
            df_pgsql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {database}')
        else:
            # print(f'Table loaded into dataframe from database: {database}')
            pass

        return df_pgsql

    def execute_pg_sql(self, sql):
        """
        connect to postgresql database and execute sql
        :param sql: string with sql query
        :return: None
        """
        cred = self.parse_db_credentials
        pgdatabase = cred['dbname']
        try:
            with pg.connect(
                    host=cred['host'],
                    port=cred['port'],
                    database=cred['dbname'],
                    user=cred['usr'],
                    password=cred['pw']
            ) as con:
                with con.cursor() as cur:
                    cur.execute(sql)
                    con.commit()
        except Exception as e:
            print(f'Unable to execute sql: {pgdatabase}:')
            print(e)
        else:
            del cred


class Database2:
    """Class handles all database related function"""
    def __init__(self, database_name, usr):
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.database_name = database_name.upper()
        self.usr_read, self.pw_read, self.host, self.port, self.database = self.parse_db_credentials()
        self.connection = self.connect_to_pg_db()

    def parse_db_credentials(self):
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.database_name}']['userid']
        pw_read = config[f'{self.database_name}']['password']
        host = config[f'{self.database_name}']['host']
        port = config[f'{self.database_name}']['port']
        dbname = config[f'{self.database_name}']['databasename']

        return usr_read, pw_read, host, port, dbname

    def connect_to_pg_db(self):
        try:
            pg_con = pg.connect(
                host=self.host,
                port=self.port,
                database=self.database,
                user=self.usr_read,
                password=self.pw_read
            )
        except Exception as e:
            print(e)
            raise ConnectionError('Could not connect to database')
        else:
            print('Connected to database: ' + self.database)

        return pg_con


class FohmLayer:

    def __init__(self, schema, table):
        self.schema = schema
        self.table = table
        d = Database(database_name='FOHM', usr='writer')
        self.con_pg = d.connection
        self.cur = self.con_pg.cursor()

    def test_production_table(self, prod_table, temp_table):
        sql_test_prod = f'''
            pg_total_relation_size('{prod_table}') AS size_prod
        '''

        sql_test_temp = f'''
            pg_total_relation_size('{temp_table}') AS size_temp
        '''

        with self.con_pg as con:
            try:
                df_prod = pd.read_sql_query(sql_test_prod, con=con)
                size_prod = df_prod['size_prod'].iloc[0]
            except:
                size_prod = 0

            try:
                df_temp = pd.read_sql_query(sql_test_temp, con=con)
                size_temp = df_temp['size_temp'].iloc[0]
            except:
                size_temp = 0

            if size_prod <= size_temp:
                update_table = True
            else:
                update_table = False

        return update_table

    def update_production_table(self):
        print(f'Create Production table from the temporary table:{self.table}...')
        table_name_opr = self.table
        table_name_opr = table_name_opr.replace('_temp', '')

        sql_opr = f'''
            BEGIN;
    
                DROP TABLE IF EXISTS {self.schema}.{table_name_opr} CASCADE;
    
                CREATE TABLE {self.schema}.{table_name_opr} AS
                    (
                        SELECT *
                        FROM {self.schema}.{self.table}
                    )
                ;
    
            COMMIT;
        '''

        update_table = self.test_production_table(f'{self.schema}.{table_name_opr}', f'{self.schema}.{self.table}')
        if update_table:
            self.cur.execute(sql_opr)
            print(sql_opr)
            print(f'Production table from the temporary table:{self.table} created')
        else:
            print(f'Production table from the temporary table:{self.table} NOT created')

    def fetch_fohm_layer_name(self):
        print('Fetching table name from fohm layers...')
        sql_tablename = '''
            SELECT 
                table_name AS tablename
            FROM information_schema.tables
            WHERE table_schema='{schema_name}'
                AND table_type='BASE TABLE'
                AND table_name ~ '^jylland_[0-9]'
                AND table_name NOT ILIKE '%isopach'
            ORDER BY (regexp_matches(table_name,'\d+'))[1]::int
        '''

        self.cur.execute(sql_tablename)
        tablenames = self.cur.fetchall()

        print('Table name from fohm layers fetched')
        return tablenames

    def create_temp_borehole(self):
        print('Creating temporary table containing jupiter boreholes...')
        sql_temp_borehole = f'''
            DROP TABLE IF EXISTS temp_borehole;

            CREATE TEMP TABLE temp_borehole AS 
                (
                    WITH 
                        kommuner_jylland AS 
                            (
                                SELECT geom
                                FROM gisdata_fdw.kommunegraense k 
                                WHERE k.kommunenr IN 
                                    (
                                        657, 615, 707, 760, 741, 779, 
                                        671, 810, 849, 825, 820, 580, 
                                        540, 573, 575, 730, 791, 530, 
                                        561, 563, 607, 510, 621, 751, 
                                        710, 766, 661, 756, 665, 727, 
                                        740, 746, 706, 851, 813, 860, 
                                        846, 773, 840, 787, 550, 630
                                    )
                            )
                    SELECT 
                        b.boreholeid, b.geom
                    FROM jupiter_fdw.borehole b
                    INNER JOIN kommuner_jylland kj ON st_intersects(b.geom, kj.geom)
                )
            ;

            CREATE INDEX temp_borehole_geom_idx
                ON temp_borehole
                USING GIST (geom)
            ;
        '''

        self.cur.execute(sql_temp_borehole)
        print('Temporary table containing jupiter boreholes created')

    def fohm_borehole(self):

        # create temporary table containing jupiter boreholes
        self.create_temp_borehole()

        # fetch fohm layer names used for tablenames
        tablenames = self.fetch_fohm_layer_name()

        # find the elevation of each fohm layer for all jupiter borehole and combine non-zero thickness layers
        sql_union = f'''
            DROP TABLE IF EXISTS {schemaname}.{tablename};

            CREATE TABLE {schemaname}.{tablename} AS 
                (
                    WITH 
                        fohm_layer_borehole AS
                            (
        '''

        for i, row in enumerate(tablenames):

            layername = row[0]
            layer_id = int(re.search(r"\d+", layername).group())

            print(f'Processersing layer: {layername}...')

            # execute sql for a temp layer elevation-values each fohm layer with respect to jupiter boreholes
            sql_fohm = f'''
                DROP TABLE IF EXISTS boreholes_{layername};

                CREATE TEMP TABLE boreholes_{layername} AS 
                    (
                        SELECT
                            b.boreholeid,
                            '{layername}' AS layer,
                            {layer_id} AS layer_num,
                            (ST_ValueCount(ST_Clip(rast,b.geom))).value AS layer_ele
                        FROM temp_borehole b
                        INNER JOIN fohm_layer."{layername}" r ON st_intersects(r.rast, b.geom)
                    )
                ;
            '''
            self.cur.execute(sql_fohm)

            # generate the union all sql
            if i == 0:
                sql_union += f'''
                            SELECT 
                                *
                            FROM boreholes_{layername}
                '''
            else:
                sql_union += f'''
                    UNION ALL
                    SELECT 
                        *
                    FROM boreholes_{layername}
                '''

            print(f'Layer: {layername} processed')

        sql_union += f'''
                        ),
                    tmp2 AS 
                        (
                            SELECT 
                                boreholeid, 
                                layer, 
                                layer_num, 
                                lag(layer_ele) OVER (PARTITION BY boreholeid ORDER BY layer_num) AS layer_top, 
                                layer_ele AS layer_bot 
                            FROM {tablename} flb  
                            ORDER BY boreholeid, layer_num
                        ),
                    tmp3 AS 
                        (
                            SELECT 
                                *,
                                CASE
                                    WHEN layer_top < layer_bot
                                        THEN 0
                                    ELSE layer_top - layer_bot
                                    END AS layer_thick,
                                CASE
                                    WHEN lower(layer) SIMILAR to '%(sand|billund|bastrup|odderup)%'
                                        THEN 'Sand'
                                    WHEN lower(layer) SIMILAR to '%(ler|vejle_fjord|klintinghoved|arnum|maadegruppen)%'
                                        THEN 'Clay'
                                    WHEN layer ILIKE '%toerv%'
                                        THEN 'Peat'
                                    WHEN layer ILIKE '%stensalt%' 	
                                        THEN 'Halite'
                                    WHEN lower(layer) SIMILAR to '%(kalk|kridt)%'
                                        THEN 'Limestone'	
                                    ELSE NULL
                                    END AS litho,
                                CASE
                                    WHEN layer_num = 100
                                        THEN 'Post-Glacial'
                                    WHEN layer_num > 100
                                        AND layer_num <= 2400
                                        THEN 'Quaternary'
                                    WHEN layer_num >= 5100
                                        AND layer_num <= 9500
                                        THEN 'Pre-Quaternary'
                                    ELSE NULL
                                    END AS time_period
                            FROM tmp2 
                            WHERE layer_top IS NOT NULL 
                        )
                SELECT 
                    tmp3.*,
                    b.geom
                FROM tmp3
                INNER JOIN jupiter_fdw.borehole b USING (boreholeid)
                WHERE layer_thick != 0
            )
        ;
        '''
        print('\nUnion all borehole-fohm tables to one')
        print(sql_union)
        self.cur.execute(sql_union)
        self.update_production_table()
        self.con_pg.commit()
        self.cur.close()
        self.con_pg.close()


# start timer
t = time.time()

# set vars
schemaname = 'fohm_borehole'
tablename = 'fohm_layer_borehole_temp'

fb = FohmLayer(schemaname, tablename)
fb.fohm_borehole()

# end timer
elapsed = round(time.time() - t, 2)
print(f'\n{os.path.basename(__file__)} executed in {elapsed} s')
