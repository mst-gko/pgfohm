
    BEGIN;

    CREATE SCHEMA IF NOT EXISTS mstfohm;

        /*
        * model:JYLLAND
        */ 
    
        -- table: mstfohm.jylland_0050_antropocaen_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_0050_antropocaen_isopach";
        
        CREATE TABLE mstfohm."jylland_0050_antropocaen_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0000_terraen"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0050_antropocaen"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0050_antropocaen_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_0050_antropocaen_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_0050_antropocaen_isopach" DROP CONSTRAINT IF EXISTS "jylland_0050_antropocaen_isopach_pkey";
        ALTER TABLE mstfohm."jylland_0050_antropocaen_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0050_antropocaen_isopach_rastidx";
        CREATE INDEX "jylland_0050_antropocaen_isopach_rastidx" ON mstfohm."jylland_0050_antropocaen_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0050_antropocaen_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_0100_postglacial_toerv_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_0100_postglacial_toerv_isopach";
        
        CREATE TABLE mstfohm."jylland_0100_postglacial_toerv_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0050_antropocaen"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0100_postglacial_toerv"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0100_postglacial_toerv_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_0100_postglacial_toerv_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_0100_postglacial_toerv_isopach" DROP CONSTRAINT IF EXISTS "jylland_0100_postglacial_toerv_isopach_pkey";
        ALTER TABLE mstfohm."jylland_0100_postglacial_toerv_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0100_postglacial_toerv_isopach_rastidx";
        CREATE INDEX "jylland_0100_postglacial_toerv_isopach_rastidx" ON mstfohm."jylland_0100_postglacial_toerv_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0100_postglacial_toerv_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_0120_postglacial_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_0120_postglacial_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_0120_postglacial_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0100_postglacial_toerv"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0120_postglacial_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0120_postglacial_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_0120_postglacial_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_0120_postglacial_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_0120_postglacial_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_0120_postglacial_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0120_postglacial_sand_isopach_rastidx";
        CREATE INDEX "jylland_0120_postglacial_sand_isopach_rastidx" ON mstfohm."jylland_0120_postglacial_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0120_postglacial_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_0130_postglacial_littorinaler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_0130_postglacial_littorinaler_isopach";
        
        CREATE TABLE mstfohm."jylland_0130_postglacial_littorinaler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0120_postglacial_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0130_postglacial_littorinaler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0130_postglacial_littorinaler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_0130_postglacial_littorinaler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_0130_postglacial_littorinaler_isopach" DROP CONSTRAINT IF EXISTS "jylland_0130_postglacial_littorinaler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_0130_postglacial_littorinaler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0130_postglacial_littorinaler_isopach_rastidx";
        CREATE INDEX "jylland_0130_postglacial_littorinaler_isopach_rastidx" ON mstfohm."jylland_0130_postglacial_littorinaler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0130_postglacial_littorinaler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_0140_senglacial_yoldiasand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_0140_senglacial_yoldiasand_isopach";
        
        CREATE TABLE mstfohm."jylland_0140_senglacial_yoldiasand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0130_postglacial_littorinaler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0140_senglacial_yoldiasand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0140_senglacial_yoldiasand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_0140_senglacial_yoldiasand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_0140_senglacial_yoldiasand_isopach" DROP CONSTRAINT IF EXISTS "jylland_0140_senglacial_yoldiasand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_0140_senglacial_yoldiasand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0140_senglacial_yoldiasand_isopach_rastidx";
        CREATE INDEX "jylland_0140_senglacial_yoldiasand_isopach_rastidx" ON mstfohm."jylland_0140_senglacial_yoldiasand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0140_senglacial_yoldiasand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_0150_senglacial_yoldialer_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_0150_senglacial_yoldialer_isopach";
        
        CREATE TABLE mstfohm."jylland_0150_senglacial_yoldialer_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0140_senglacial_yoldiasand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0150_senglacial_yoldialer"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0150_senglacial_yoldialer_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_0150_senglacial_yoldialer_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_0150_senglacial_yoldialer_isopach" DROP CONSTRAINT IF EXISTS "jylland_0150_senglacial_yoldialer_isopach_pkey";
        ALTER TABLE mstfohm."jylland_0150_senglacial_yoldialer_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0150_senglacial_yoldialer_isopach_rastidx";
        CREATE INDEX "jylland_0150_senglacial_yoldialer_isopach_rastidx" ON mstfohm."jylland_0150_senglacial_yoldialer_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0150_senglacial_yoldialer_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_0200_kvartaer_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_0200_kvartaer_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_0200_kvartaer_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0150_senglacial_yoldialer"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0200_kvartaer_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0200_kvartaer_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_0200_kvartaer_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_0200_kvartaer_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_0200_kvartaer_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_0200_kvartaer_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0200_kvartaer_sand_isopach_rastidx";
        CREATE INDEX "jylland_0200_kvartaer_sand_isopach_rastidx" ON mstfohm."jylland_0200_kvartaer_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0200_kvartaer_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_0300_kvartaer_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_0300_kvartaer_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_0300_kvartaer_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0200_kvartaer_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0300_kvartaer_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0300_kvartaer_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_0300_kvartaer_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_0300_kvartaer_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_0300_kvartaer_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_0300_kvartaer_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0300_kvartaer_ler_isopach_rastidx";
        CREATE INDEX "jylland_0300_kvartaer_ler_isopach_rastidx" ON mstfohm."jylland_0300_kvartaer_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0300_kvartaer_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_0400_kvartaer_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_0400_kvartaer_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_0400_kvartaer_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0300_kvartaer_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0400_kvartaer_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_0400_kvartaer_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_0400_kvartaer_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_0400_kvartaer_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_0400_kvartaer_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_0400_kvartaer_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_0400_kvartaer_sand_isopach_rastidx";
        CREATE INDEX "jylland_0400_kvartaer_sand_isopach_rastidx" ON mstfohm."jylland_0400_kvartaer_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_0400_kvartaer_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_1100_kvartaer_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_1100_kvartaer_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_1100_kvartaer_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_0400_kvartaer_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_1100_kvartaer_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_1100_kvartaer_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_1100_kvartaer_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_1100_kvartaer_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_1100_kvartaer_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_1100_kvartaer_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_1100_kvartaer_ler_isopach_rastidx";
        CREATE INDEX "jylland_1100_kvartaer_ler_isopach_rastidx" ON mstfohm."jylland_1100_kvartaer_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_1100_kvartaer_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_1200_kvartaer_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_1200_kvartaer_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_1200_kvartaer_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_1100_kvartaer_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_1200_kvartaer_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_1200_kvartaer_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_1200_kvartaer_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_1200_kvartaer_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_1200_kvartaer_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_1200_kvartaer_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_1200_kvartaer_sand_isopach_rastidx";
        CREATE INDEX "jylland_1200_kvartaer_sand_isopach_rastidx" ON mstfohm."jylland_1200_kvartaer_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_1200_kvartaer_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_1300_kvartaer_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_1300_kvartaer_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_1300_kvartaer_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_1200_kvartaer_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_1300_kvartaer_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_1300_kvartaer_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_1300_kvartaer_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_1300_kvartaer_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_1300_kvartaer_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_1300_kvartaer_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_1300_kvartaer_ler_isopach_rastidx";
        CREATE INDEX "jylland_1300_kvartaer_ler_isopach_rastidx" ON mstfohm."jylland_1300_kvartaer_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_1300_kvartaer_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_1400_kvartaer_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_1400_kvartaer_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_1400_kvartaer_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_1300_kvartaer_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_1400_kvartaer_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_1400_kvartaer_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_1400_kvartaer_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_1400_kvartaer_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_1400_kvartaer_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_1400_kvartaer_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_1400_kvartaer_sand_isopach_rastidx";
        CREATE INDEX "jylland_1400_kvartaer_sand_isopach_rastidx" ON mstfohm."jylland_1400_kvartaer_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_1400_kvartaer_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_1500_kvartaer_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_1500_kvartaer_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_1500_kvartaer_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_1400_kvartaer_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_1500_kvartaer_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_1500_kvartaer_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_1500_kvartaer_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_1500_kvartaer_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_1500_kvartaer_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_1500_kvartaer_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_1500_kvartaer_ler_isopach_rastidx";
        CREATE INDEX "jylland_1500_kvartaer_ler_isopach_rastidx" ON mstfohm."jylland_1500_kvartaer_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_1500_kvartaer_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_2100_kvartaer_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_2100_kvartaer_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_2100_kvartaer_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_1500_kvartaer_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_2100_kvartaer_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_2100_kvartaer_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_2100_kvartaer_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_2100_kvartaer_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_2100_kvartaer_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_2100_kvartaer_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_2100_kvartaer_sand_isopach_rastidx";
        CREATE INDEX "jylland_2100_kvartaer_sand_isopach_rastidx" ON mstfohm."jylland_2100_kvartaer_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_2100_kvartaer_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_2200_kvartaer_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_2200_kvartaer_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_2200_kvartaer_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_2100_kvartaer_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_2200_kvartaer_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_2200_kvartaer_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_2200_kvartaer_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_2200_kvartaer_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_2200_kvartaer_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_2200_kvartaer_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_2200_kvartaer_ler_isopach_rastidx";
        CREATE INDEX "jylland_2200_kvartaer_ler_isopach_rastidx" ON mstfohm."jylland_2200_kvartaer_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_2200_kvartaer_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_2300_kvartaer_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_2300_kvartaer_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_2300_kvartaer_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_2200_kvartaer_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_2300_kvartaer_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_2300_kvartaer_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_2300_kvartaer_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_2300_kvartaer_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_2300_kvartaer_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_2300_kvartaer_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_2300_kvartaer_sand_isopach_rastidx";
        CREATE INDEX "jylland_2300_kvartaer_sand_isopach_rastidx" ON mstfohm."jylland_2300_kvartaer_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_2300_kvartaer_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_2400_kvartaer_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_2400_kvartaer_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_2400_kvartaer_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_2300_kvartaer_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_2400_kvartaer_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_2400_kvartaer_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_2400_kvartaer_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_2400_kvartaer_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_2400_kvartaer_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_2400_kvartaer_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_2400_kvartaer_ler_isopach_rastidx";
        CREATE INDEX "jylland_2400_kvartaer_ler_isopach_rastidx" ON mstfohm."jylland_2400_kvartaer_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_2400_kvartaer_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_5100_maadegruppen_gram_og_hodde_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_5100_maadegruppen_gram_og_hodde_isopach";
        
        CREATE TABLE mstfohm."jylland_5100_maadegruppen_gram_og_hodde_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_2400_kvartaer_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5100_maadegruppen_gram_og_hodde"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5100_maadegruppen_gram_og_hodde_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_5100_maadegruppen_gram_og_hodde_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_5100_maadegruppen_gram_og_hodde_isopach" DROP CONSTRAINT IF EXISTS "jylland_5100_maadegruppen_gram_og_hodde_isopach_pkey";
        ALTER TABLE mstfohm."jylland_5100_maadegruppen_gram_og_hodde_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5100_maadegruppen_gram_og_hodde_isopach_rastidx";
        CREATE INDEX "jylland_5100_maadegruppen_gram_og_hodde_isopach_rastidx" ON mstfohm."jylland_5100_maadegruppen_gram_og_hodde_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5100_maadegruppen_gram_og_hodde_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_5200_øvre_odderup_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_5200_øvre_odderup_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_5200_øvre_odderup_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5100_maadegruppen_gram_og_hodde"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5200_øvre_odderup_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5200_øvre_odderup_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_5200_øvre_odderup_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_5200_øvre_odderup_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_5200_øvre_odderup_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_5200_øvre_odderup_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5200_øvre_odderup_sand_isopach_rastidx";
        CREATE INDEX "jylland_5200_øvre_odderup_sand_isopach_rastidx" ON mstfohm."jylland_5200_øvre_odderup_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5200_øvre_odderup_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_5300_øvre_arnum_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_5300_øvre_arnum_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_5300_øvre_arnum_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5200_øvre_odderup_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5300_øvre_arnum_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5300_øvre_arnum_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_5300_øvre_arnum_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_5300_øvre_arnum_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_5300_øvre_arnum_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_5300_øvre_arnum_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5300_øvre_arnum_ler_isopach_rastidx";
        CREATE INDEX "jylland_5300_øvre_arnum_ler_isopach_rastidx" ON mstfohm."jylland_5300_øvre_arnum_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5300_øvre_arnum_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_5400_nedre_odderup_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_5400_nedre_odderup_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_5400_nedre_odderup_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5300_øvre_arnum_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5400_nedre_odderup_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5400_nedre_odderup_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_5400_nedre_odderup_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_5400_nedre_odderup_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_5400_nedre_odderup_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_5400_nedre_odderup_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5400_nedre_odderup_sand_isopach_rastidx";
        CREATE INDEX "jylland_5400_nedre_odderup_sand_isopach_rastidx" ON mstfohm."jylland_5400_nedre_odderup_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5400_nedre_odderup_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_5500_nedre_arnum_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_5500_nedre_arnum_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_5500_nedre_arnum_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5400_nedre_odderup_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5500_nedre_arnum_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5500_nedre_arnum_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_5500_nedre_arnum_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_5500_nedre_arnum_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_5500_nedre_arnum_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_5500_nedre_arnum_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5500_nedre_arnum_ler_isopach_rastidx";
        CREATE INDEX "jylland_5500_nedre_arnum_ler_isopach_rastidx" ON mstfohm."jylland_5500_nedre_arnum_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5500_nedre_arnum_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_5600_bastrup_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_5600_bastrup_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_5600_bastrup_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5500_nedre_arnum_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5600_bastrup_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5600_bastrup_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_5600_bastrup_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_5600_bastrup_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_5600_bastrup_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_5600_bastrup_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5600_bastrup_sand_isopach_rastidx";
        CREATE INDEX "jylland_5600_bastrup_sand_isopach_rastidx" ON mstfohm."jylland_5600_bastrup_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5600_bastrup_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_5700_klintinghoved_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_5700_klintinghoved_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_5700_klintinghoved_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5600_bastrup_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5700_klintinghoved_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5700_klintinghoved_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_5700_klintinghoved_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_5700_klintinghoved_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_5700_klintinghoved_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_5700_klintinghoved_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5700_klintinghoved_ler_isopach_rastidx";
        CREATE INDEX "jylland_5700_klintinghoved_ler_isopach_rastidx" ON mstfohm."jylland_5700_klintinghoved_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5700_klintinghoved_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_5800_bastrup_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_5800_bastrup_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_5800_bastrup_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5700_klintinghoved_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5800_bastrup_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5800_bastrup_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_5800_bastrup_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_5800_bastrup_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_5800_bastrup_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_5800_bastrup_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5800_bastrup_sand_isopach_rastidx";
        CREATE INDEX "jylland_5800_bastrup_sand_isopach_rastidx" ON mstfohm."jylland_5800_bastrup_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5800_bastrup_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_5900_klintinghoved_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_5900_klintinghoved_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_5900_klintinghoved_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5800_bastrup_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5900_klintinghoved_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_5900_klintinghoved_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_5900_klintinghoved_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_5900_klintinghoved_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_5900_klintinghoved_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_5900_klintinghoved_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_5900_klintinghoved_ler_isopach_rastidx";
        CREATE INDEX "jylland_5900_klintinghoved_ler_isopach_rastidx" ON mstfohm."jylland_5900_klintinghoved_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_5900_klintinghoved_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_6000_bastrup_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_6000_bastrup_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_6000_bastrup_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_5900_klintinghoved_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6000_bastrup_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6000_bastrup_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_6000_bastrup_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_6000_bastrup_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_6000_bastrup_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_6000_bastrup_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6000_bastrup_sand_isopach_rastidx";
        CREATE INDEX "jylland_6000_bastrup_sand_isopach_rastidx" ON mstfohm."jylland_6000_bastrup_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6000_bastrup_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_6100_klintinghoved_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_6100_klintinghoved_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_6100_klintinghoved_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6000_bastrup_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6100_klintinghoved_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6100_klintinghoved_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_6100_klintinghoved_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_6100_klintinghoved_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_6100_klintinghoved_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_6100_klintinghoved_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6100_klintinghoved_ler_isopach_rastidx";
        CREATE INDEX "jylland_6100_klintinghoved_ler_isopach_rastidx" ON mstfohm."jylland_6100_klintinghoved_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6100_klintinghoved_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_6200_bastrup_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_6200_bastrup_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_6200_bastrup_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6100_klintinghoved_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6200_bastrup_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6200_bastrup_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_6200_bastrup_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_6200_bastrup_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_6200_bastrup_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_6200_bastrup_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6200_bastrup_sand_isopach_rastidx";
        CREATE INDEX "jylland_6200_bastrup_sand_isopach_rastidx" ON mstfohm."jylland_6200_bastrup_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6200_bastrup_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_6300_klintinghoved_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_6300_klintinghoved_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_6300_klintinghoved_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6200_bastrup_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6300_klintinghoved_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6300_klintinghoved_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_6300_klintinghoved_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_6300_klintinghoved_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_6300_klintinghoved_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_6300_klintinghoved_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6300_klintinghoved_ler_isopach_rastidx";
        CREATE INDEX "jylland_6300_klintinghoved_ler_isopach_rastidx" ON mstfohm."jylland_6300_klintinghoved_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6300_klintinghoved_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_6400_bastrup_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_6400_bastrup_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_6400_bastrup_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6300_klintinghoved_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6400_bastrup_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6400_bastrup_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_6400_bastrup_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_6400_bastrup_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_6400_bastrup_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_6400_bastrup_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6400_bastrup_sand_isopach_rastidx";
        CREATE INDEX "jylland_6400_bastrup_sand_isopach_rastidx" ON mstfohm."jylland_6400_bastrup_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6400_bastrup_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_6500_klintinghoved_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_6500_klintinghoved_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_6500_klintinghoved_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6400_bastrup_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6500_klintinghoved_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6500_klintinghoved_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_6500_klintinghoved_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_6500_klintinghoved_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_6500_klintinghoved_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_6500_klintinghoved_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6500_klintinghoved_ler_isopach_rastidx";
        CREATE INDEX "jylland_6500_klintinghoved_ler_isopach_rastidx" ON mstfohm."jylland_6500_klintinghoved_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6500_klintinghoved_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_6600_bastrup_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_6600_bastrup_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_6600_bastrup_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6500_klintinghoved_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6600_bastrup_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6600_bastrup_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_6600_bastrup_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_6600_bastrup_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_6600_bastrup_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_6600_bastrup_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6600_bastrup_sand_isopach_rastidx";
        CREATE INDEX "jylland_6600_bastrup_sand_isopach_rastidx" ON mstfohm."jylland_6600_bastrup_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6600_bastrup_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_6700_klintinghoved_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_6700_klintinghoved_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_6700_klintinghoved_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6600_bastrup_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6700_klintinghoved_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6700_klintinghoved_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_6700_klintinghoved_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_6700_klintinghoved_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_6700_klintinghoved_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_6700_klintinghoved_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6700_klintinghoved_ler_isopach_rastidx";
        CREATE INDEX "jylland_6700_klintinghoved_ler_isopach_rastidx" ON mstfohm."jylland_6700_klintinghoved_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6700_klintinghoved_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_6800_billund_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_6800_billund_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_6800_billund_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6700_klintinghoved_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6800_billund_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6800_billund_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_6800_billund_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_6800_billund_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_6800_billund_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_6800_billund_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6800_billund_sand_isopach_rastidx";
        CREATE INDEX "jylland_6800_billund_sand_isopach_rastidx" ON mstfohm."jylland_6800_billund_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6800_billund_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_6900_vejle_fjord_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_6900_vejle_fjord_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_6900_vejle_fjord_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6800_billund_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6900_vejle_fjord_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_6900_vejle_fjord_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_6900_vejle_fjord_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_6900_vejle_fjord_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_6900_vejle_fjord_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_6900_vejle_fjord_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_6900_vejle_fjord_ler_isopach_rastidx";
        CREATE INDEX "jylland_6900_vejle_fjord_ler_isopach_rastidx" ON mstfohm."jylland_6900_vejle_fjord_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_6900_vejle_fjord_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_7000_billund_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_7000_billund_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_7000_billund_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_6900_vejle_fjord_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7000_billund_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7000_billund_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_7000_billund_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_7000_billund_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_7000_billund_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_7000_billund_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7000_billund_sand_isopach_rastidx";
        CREATE INDEX "jylland_7000_billund_sand_isopach_rastidx" ON mstfohm."jylland_7000_billund_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7000_billund_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_7100_vejle_fjord_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_7100_vejle_fjord_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_7100_vejle_fjord_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7000_billund_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7100_vejle_fjord_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7100_vejle_fjord_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_7100_vejle_fjord_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_7100_vejle_fjord_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_7100_vejle_fjord_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_7100_vejle_fjord_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7100_vejle_fjord_ler_isopach_rastidx";
        CREATE INDEX "jylland_7100_vejle_fjord_ler_isopach_rastidx" ON mstfohm."jylland_7100_vejle_fjord_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7100_vejle_fjord_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_7200_billund_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_7200_billund_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_7200_billund_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7100_vejle_fjord_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7200_billund_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7200_billund_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_7200_billund_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_7200_billund_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_7200_billund_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_7200_billund_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7200_billund_sand_isopach_rastidx";
        CREATE INDEX "jylland_7200_billund_sand_isopach_rastidx" ON mstfohm."jylland_7200_billund_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7200_billund_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_7300_vejle_fjord_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_7300_vejle_fjord_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_7300_vejle_fjord_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7200_billund_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7300_vejle_fjord_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7300_vejle_fjord_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_7300_vejle_fjord_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_7300_vejle_fjord_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_7300_vejle_fjord_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_7300_vejle_fjord_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7300_vejle_fjord_ler_isopach_rastidx";
        CREATE INDEX "jylland_7300_vejle_fjord_ler_isopach_rastidx" ON mstfohm."jylland_7300_vejle_fjord_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7300_vejle_fjord_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_7400_billund_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_7400_billund_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_7400_billund_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7300_vejle_fjord_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7400_billund_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7400_billund_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_7400_billund_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_7400_billund_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_7400_billund_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_7400_billund_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7400_billund_sand_isopach_rastidx";
        CREATE INDEX "jylland_7400_billund_sand_isopach_rastidx" ON mstfohm."jylland_7400_billund_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7400_billund_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_7500_vejle_fjord_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_7500_vejle_fjord_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_7500_vejle_fjord_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7400_billund_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7500_vejle_fjord_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7500_vejle_fjord_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_7500_vejle_fjord_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_7500_vejle_fjord_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_7500_vejle_fjord_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_7500_vejle_fjord_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7500_vejle_fjord_ler_isopach_rastidx";
        CREATE INDEX "jylland_7500_vejle_fjord_ler_isopach_rastidx" ON mstfohm."jylland_7500_vejle_fjord_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7500_vejle_fjord_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_7600_billund_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_7600_billund_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_7600_billund_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7500_vejle_fjord_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7600_billund_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7600_billund_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_7600_billund_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_7600_billund_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_7600_billund_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_7600_billund_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7600_billund_sand_isopach_rastidx";
        CREATE INDEX "jylland_7600_billund_sand_isopach_rastidx" ON mstfohm."jylland_7600_billund_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7600_billund_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_7700_vejle_fjord_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_7700_vejle_fjord_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_7700_vejle_fjord_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7600_billund_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7700_vejle_fjord_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7700_vejle_fjord_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_7700_vejle_fjord_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_7700_vejle_fjord_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_7700_vejle_fjord_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_7700_vejle_fjord_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7700_vejle_fjord_ler_isopach_rastidx";
        CREATE INDEX "jylland_7700_vejle_fjord_ler_isopach_rastidx" ON mstfohm."jylland_7700_vejle_fjord_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7700_vejle_fjord_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_7800_billund_sand_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_7800_billund_sand_isopach";
        
        CREATE TABLE mstfohm."jylland_7800_billund_sand_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7700_vejle_fjord_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7800_billund_sand"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_7800_billund_sand_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_7800_billund_sand_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_7800_billund_sand_isopach" DROP CONSTRAINT IF EXISTS "jylland_7800_billund_sand_isopach_pkey";
        ALTER TABLE mstfohm."jylland_7800_billund_sand_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_7800_billund_sand_isopach_rastidx";
        CREATE INDEX "jylland_7800_billund_sand_isopach_rastidx" ON mstfohm."jylland_7800_billund_sand_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_7800_billund_sand_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_8000_palaeogen_ler_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_8000_palaeogen_ler_isopach";
        
        CREATE TABLE mstfohm."jylland_8000_palaeogen_ler_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_7800_billund_sand"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_8000_palaeogen_ler"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_8000_palaeogen_ler_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_8000_palaeogen_ler_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_8000_palaeogen_ler_isopach" DROP CONSTRAINT IF EXISTS "jylland_8000_palaeogen_ler_isopach_pkey";
        ALTER TABLE mstfohm."jylland_8000_palaeogen_ler_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_8000_palaeogen_ler_isopach_rastidx";
        CREATE INDEX "jylland_8000_palaeogen_ler_isopach_rastidx" ON mstfohm."jylland_8000_palaeogen_ler_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_8000_palaeogen_ler_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_8500_danien_kalk_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_8500_danien_kalk_isopach";
        
        CREATE TABLE mstfohm."jylland_8500_danien_kalk_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_8000_palaeogen_ler"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_8500_danien_kalk"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_8500_danien_kalk_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_8500_danien_kalk_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_8500_danien_kalk_isopach" DROP CONSTRAINT IF EXISTS "jylland_8500_danien_kalk_isopach_pkey";
        ALTER TABLE mstfohm."jylland_8500_danien_kalk_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_8500_danien_kalk_isopach_rastidx";
        CREATE INDEX "jylland_8500_danien_kalk_isopach_rastidx" ON mstfohm."jylland_8500_danien_kalk_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_8500_danien_kalk_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.jylland_9000_skrivekridt_isopach

        DROP TABLE IF EXISTS mstfohm."jylland_9000_skrivekridt_isopach";
        
        CREATE TABLE mstfohm."jylland_9000_skrivekridt_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_8500_danien_kalk"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_9000_skrivekridt"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."jylland_9000_skrivekridt_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of jylland_9000_skrivekridt_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."jylland_9000_skrivekridt_isopach" DROP CONSTRAINT IF EXISTS "jylland_9000_skrivekridt_isopach_pkey";
        ALTER TABLE mstfohm."jylland_9000_skrivekridt_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "jylland_9000_skrivekridt_isopach_rastidx";
        CREATE INDEX "jylland_9000_skrivekridt_isopach_rastidx" ON mstfohm."jylland_9000_skrivekridt_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'jylland_9000_skrivekridt_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.9999_dummy_layer_isopach

        DROP TABLE IF EXISTS mstfohm."9999_dummy_layer_isopach";
        
        CREATE TABLE mstfohm."9999_dummy_layer_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_9000_skrivekridt"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."jylland_9500_stensalt"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."9999_dummy_layer_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of 9999_dummy_layer_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."9999_dummy_layer_isopach" DROP CONSTRAINT IF EXISTS "9999_dummy_layer_isopach_pkey";
        ALTER TABLE mstfohm."9999_dummy_layer_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "9999_dummy_layer_isopach_rastidx";
        CREATE INDEX "9999_dummy_layer_isopach_rastidx" ON mstfohm."9999_dummy_layer_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, '9999_dummy_layer_isopach'::name, 'rast'::name);

        
        /*
        * model:SJAELLAND
        */ 
    
        -- table: mstfohm.sjaelland_0002_kl1_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0002_kl1_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0002_kl1_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0001_topo"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0002_kl1"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0002_kl1_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0002_kl1_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0002_kl1_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0002_kl1_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0002_kl1_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0002_kl1_isopach_rastidx";
        CREATE INDEX "sjaelland_0002_kl1_isopach_rastidx" ON mstfohm."sjaelland_0002_kl1_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0002_kl1_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.sjaelland_0003_ks1_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0003_ks1_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0003_ks1_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0002_kl1"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0003_ks1"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0003_ks1_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0003_ks1_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0003_ks1_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0003_ks1_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0003_ks1_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0003_ks1_isopach_rastidx";
        CREATE INDEX "sjaelland_0003_ks1_isopach_rastidx" ON mstfohm."sjaelland_0003_ks1_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0003_ks1_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.sjaelland_0004_kl2_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0004_kl2_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0004_kl2_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0003_ks1"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0004_kl2"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0004_kl2_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0004_kl2_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0004_kl2_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0004_kl2_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0004_kl2_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0004_kl2_isopach_rastidx";
        CREATE INDEX "sjaelland_0004_kl2_isopach_rastidx" ON mstfohm."sjaelland_0004_kl2_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0004_kl2_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.sjaelland_0005_ks2_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0005_ks2_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0005_ks2_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0004_kl2"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0005_ks2"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0005_ks2_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0005_ks2_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0005_ks2_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0005_ks2_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0005_ks2_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0005_ks2_isopach_rastidx";
        CREATE INDEX "sjaelland_0005_ks2_isopach_rastidx" ON mstfohm."sjaelland_0005_ks2_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0005_ks2_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.sjaelland_0006_kl3_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0006_kl3_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0006_kl3_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0005_ks2"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0006_kl3"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0006_kl3_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0006_kl3_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0006_kl3_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0006_kl3_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0006_kl3_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0006_kl3_isopach_rastidx";
        CREATE INDEX "sjaelland_0006_kl3_isopach_rastidx" ON mstfohm."sjaelland_0006_kl3_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0006_kl3_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.sjaelland_0007_ks3_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0007_ks3_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0007_ks3_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0006_kl3"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0007_ks3"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0007_ks3_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0007_ks3_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0007_ks3_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0007_ks3_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0007_ks3_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0007_ks3_isopach_rastidx";
        CREATE INDEX "sjaelland_0007_ks3_isopach_rastidx" ON mstfohm."sjaelland_0007_ks3_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0007_ks3_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.sjaelland_0008_kl4_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0008_kl4_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0008_kl4_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0007_ks3"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0008_kl4"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0008_kl4_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0008_kl4_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0008_kl4_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0008_kl4_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0008_kl4_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0008_kl4_isopach_rastidx";
        CREATE INDEX "sjaelland_0008_kl4_isopach_rastidx" ON mstfohm."sjaelland_0008_kl4_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0008_kl4_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.sjaelland_0009_ks4_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0009_ks4_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0009_ks4_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0008_kl4"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0009_ks4"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0009_ks4_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0009_ks4_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0009_ks4_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0009_ks4_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0009_ks4_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0009_ks4_isopach_rastidx";
        CREATE INDEX "sjaelland_0009_ks4_isopach_rastidx" ON mstfohm."sjaelland_0009_ks4_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0009_ks4_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.sjaelland_0010_kl5_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0010_kl5_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0010_kl5_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0009_ks4"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0010_kl5"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0010_kl5_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0010_kl5_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0010_kl5_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0010_kl5_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0010_kl5_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0010_kl5_isopach_rastidx";
        CREATE INDEX "sjaelland_0010_kl5_isopach_rastidx" ON mstfohm."sjaelland_0010_kl5_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0010_kl5_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.sjaelland_0011_pl1_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0011_pl1_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0011_pl1_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0010_kl5"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0011_pl1"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0011_pl1_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0011_pl1_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0011_pl1_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0011_pl1_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0011_pl1_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0011_pl1_isopach_rastidx";
        CREATE INDEX "sjaelland_0011_pl1_isopach_rastidx" ON mstfohm."sjaelland_0011_pl1_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0011_pl1_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.sjaelland_0012_gk1b_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0012_gk1b_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0012_gk1b_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0011_pl1"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0012_gk1b"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0012_gk1b_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0012_gk1b_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0012_gk1b_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0012_gk1b_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0012_gk1b_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0012_gk1b_isopach_rastidx";
        CREATE INDEX "sjaelland_0012_gk1b_isopach_rastidx" ON mstfohm."sjaelland_0012_gk1b_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0012_gk1b_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.sjaelland_0013_dk1b_isopach

        DROP TABLE IF EXISTS mstfohm."sjaelland_0013_dk1b_isopach";
        
        CREATE TABLE mstfohm."sjaelland_0013_dk1b_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0012_gk1b"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0013_dk1b"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0013_dk1b_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of sjaelland_0013_dk1b_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."sjaelland_0013_dk1b_isopach" DROP CONSTRAINT IF EXISTS "sjaelland_0013_dk1b_isopach_pkey";
        ALTER TABLE mstfohm."sjaelland_0013_dk1b_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0013_dk1b_isopach_rastidx";
        CREATE INDEX "sjaelland_0013_dk1b_isopach_rastidx" ON mstfohm."sjaelland_0013_dk1b_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0013_dk1b_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.9999_dummy_layer_isopach

        DROP TABLE IF EXISTS mstfohm."9999_dummy_layer_isopach";
        
        CREATE TABLE mstfohm."9999_dummy_layer_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0013_dk1b"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."sjaelland_0014_sk1"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."9999_dummy_layer_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of 9999_dummy_layer_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."9999_dummy_layer_isopach" DROP CONSTRAINT IF EXISTS "9999_dummy_layer_isopach_pkey";
        ALTER TABLE mstfohm."9999_dummy_layer_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "9999_dummy_layer_isopach_rastidx";
        CREATE INDEX "9999_dummy_layer_isopach_rastidx" ON mstfohm."9999_dummy_layer_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, '9999_dummy_layer_isopach'::name, 'rast'::name);

        
        /*
        * model:FYN
        */ 
    
        -- table: mstfohm.fyn_0002_kl1_isopach

        DROP TABLE IF EXISTS mstfohm."fyn_0002_kl1_isopach";
        
        CREATE TABLE mstfohm."fyn_0002_kl1_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0001_topo"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0002_kl1"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0002_kl1_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of fyn_0002_kl1_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."fyn_0002_kl1_isopach" DROP CONSTRAINT IF EXISTS "fyn_0002_kl1_isopach_pkey";
        ALTER TABLE mstfohm."fyn_0002_kl1_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0002_kl1_isopach_rastidx";
        CREATE INDEX "fyn_0002_kl1_isopach_rastidx" ON mstfohm."fyn_0002_kl1_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0002_kl1_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.fyn_0003_ks1_isopach

        DROP TABLE IF EXISTS mstfohm."fyn_0003_ks1_isopach";
        
        CREATE TABLE mstfohm."fyn_0003_ks1_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0002_kl1"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0003_ks1"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0003_ks1_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of fyn_0003_ks1_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."fyn_0003_ks1_isopach" DROP CONSTRAINT IF EXISTS "fyn_0003_ks1_isopach_pkey";
        ALTER TABLE mstfohm."fyn_0003_ks1_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0003_ks1_isopach_rastidx";
        CREATE INDEX "fyn_0003_ks1_isopach_rastidx" ON mstfohm."fyn_0003_ks1_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0003_ks1_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.fyn_0004_kl2_isopach

        DROP TABLE IF EXISTS mstfohm."fyn_0004_kl2_isopach";
        
        CREATE TABLE mstfohm."fyn_0004_kl2_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0003_ks1"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0004_kl2"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0004_kl2_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of fyn_0004_kl2_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."fyn_0004_kl2_isopach" DROP CONSTRAINT IF EXISTS "fyn_0004_kl2_isopach_pkey";
        ALTER TABLE mstfohm."fyn_0004_kl2_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0004_kl2_isopach_rastidx";
        CREATE INDEX "fyn_0004_kl2_isopach_rastidx" ON mstfohm."fyn_0004_kl2_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0004_kl2_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.fyn_0005_ks2_isopach

        DROP TABLE IF EXISTS mstfohm."fyn_0005_ks2_isopach";
        
        CREATE TABLE mstfohm."fyn_0005_ks2_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0004_kl2"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0005_ks2"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0005_ks2_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of fyn_0005_ks2_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."fyn_0005_ks2_isopach" DROP CONSTRAINT IF EXISTS "fyn_0005_ks2_isopach_pkey";
        ALTER TABLE mstfohm."fyn_0005_ks2_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0005_ks2_isopach_rastidx";
        CREATE INDEX "fyn_0005_ks2_isopach_rastidx" ON mstfohm."fyn_0005_ks2_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0005_ks2_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.fyn_0006_kl3_isopach

        DROP TABLE IF EXISTS mstfohm."fyn_0006_kl3_isopach";
        
        CREATE TABLE mstfohm."fyn_0006_kl3_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0005_ks2"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0006_kl3"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0006_kl3_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of fyn_0006_kl3_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."fyn_0006_kl3_isopach" DROP CONSTRAINT IF EXISTS "fyn_0006_kl3_isopach_pkey";
        ALTER TABLE mstfohm."fyn_0006_kl3_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0006_kl3_isopach_rastidx";
        CREATE INDEX "fyn_0006_kl3_isopach_rastidx" ON mstfohm."fyn_0006_kl3_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0006_kl3_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.fyn_0007_ks3_isopach

        DROP TABLE IF EXISTS mstfohm."fyn_0007_ks3_isopach";
        
        CREATE TABLE mstfohm."fyn_0007_ks3_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0006_kl3"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0007_ks3"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0007_ks3_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of fyn_0007_ks3_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."fyn_0007_ks3_isopach" DROP CONSTRAINT IF EXISTS "fyn_0007_ks3_isopach_pkey";
        ALTER TABLE mstfohm."fyn_0007_ks3_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0007_ks3_isopach_rastidx";
        CREATE INDEX "fyn_0007_ks3_isopach_rastidx" ON mstfohm."fyn_0007_ks3_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0007_ks3_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.fyn_0008_kl4_isopach

        DROP TABLE IF EXISTS mstfohm."fyn_0008_kl4_isopach";
        
        CREATE TABLE mstfohm."fyn_0008_kl4_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0007_ks3"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0008_kl4"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0008_kl4_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of fyn_0008_kl4_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."fyn_0008_kl4_isopach" DROP CONSTRAINT IF EXISTS "fyn_0008_kl4_isopach_pkey";
        ALTER TABLE mstfohm."fyn_0008_kl4_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0008_kl4_isopach_rastidx";
        CREATE INDEX "fyn_0008_kl4_isopach_rastidx" ON mstfohm."fyn_0008_kl4_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0008_kl4_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.fyn_0009_pl1_isopach

        DROP TABLE IF EXISTS mstfohm."fyn_0009_pl1_isopach";
        
        CREATE TABLE mstfohm."fyn_0009_pl1_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0008_kl4"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0009_pl1"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."fyn_0009_pl1_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of fyn_0009_pl1_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."fyn_0009_pl1_isopach" DROP CONSTRAINT IF EXISTS "fyn_0009_pl1_isopach_pkey";
        ALTER TABLE mstfohm."fyn_0009_pl1_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "fyn_0009_pl1_isopach_rastidx";
        CREATE INDEX "fyn_0009_pl1_isopach_rastidx" ON mstfohm."fyn_0009_pl1_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, 'fyn_0009_pl1_isopach'::name, 'rast'::name);

        
        -- table: mstfohm.9999_dummy_layer_isopach

        DROP TABLE IF EXISTS mstfohm."9999_dummy_layer_isopach";
        
        CREATE TABLE mstfohm."9999_dummy_layer_isopach" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0009_pl1"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM mstfohm."fyn_0010_kalk"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."9999_dummy_layer_isopach" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of 9999_dummy_layer_isopach'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE mstfohm."9999_dummy_layer_isopach" DROP CONSTRAINT IF EXISTS "9999_dummy_layer_isopach_pkey";
        ALTER TABLE mstfohm."9999_dummy_layer_isopach" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "9999_dummy_layer_isopach_rastidx";
        CREATE INDEX "9999_dummy_layer_isopach_rastidx" ON mstfohm."9999_dummy_layer_isopach" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('mstfohm'::name, '9999_dummy_layer_isopach'::name, 'rast'::name);

        
COMMIT;

GRANT SELECT ON ALL TABLES IN SCHEMA mstfohm TO jupiterrole;
GRANT USAGE ON SCHEMA mstfohm TO jupiterrole;
