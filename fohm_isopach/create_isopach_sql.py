import time
import os
import configparser
import io

import psycopg2 as pg


class Database:
    """Class handles all database related function"""
    def __init__(self, database_name, usr):
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.database_name = database_name.upper()
        self.usr_read, self.pw_read, self.host, self.port, self.database = self.parse_db_credentials()
        self.connection = self.connect_to_pg_db()

    def parse_db_credentials(self):
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.database_name}']['userid']
        pw_read = config[f'{self.database_name}']['password']
        host = config[f'{self.database_name}']['host']
        port = config[f'{self.database_name}']['port']
        dbname = config[f'{self.database_name}']['databasename']

        return usr_read, pw_read, host, port, dbname

    def connect_to_pg_db(self):
        try:
            pg_con = pg.connect(
                host=self.host,
                port=self.port,
                database=self.database,
                user=self.usr_read,
                password=self.pw_read
            )
        except Exception as e:
            print(e)
            raise ConnectionError('Could not connect to database')
        else:
            print('Connected to database: ' + self.database)

        return pg_con

# start timer
t = time.time()

# set vars
schema_name = 'mstfohm'

models = [
    ('JYLLAND', "table_name ~ '^jylland_[0-9]'"),
    ('SJAELLAND', "table_name ~ '^sjaelland_[0-9]'"),
    ('FYN', "table_name ~ '^fyn_[0-9]'")
]

sql_fohm_thickness_combined = f"""
    BEGIN;

    CREATE SCHEMA IF NOT EXISTS {schema_name};
"""

for model in models:
    model_name = model[0]
    where_clause = model[1]

    sql_fohm_thickness_combined += f'''
        /*
        * model:{model_name}
        */ 
    '''


    sql_tablename = f'''
        SELECT table_name AS tablename
        FROM information_schema.tables
        WHERE table_schema='{schema_name}'
            AND table_type='BASE TABLE'
            AND {where_clause}
            AND table_name NOT ILIKE '%isopach'
        ORDER BY (regexp_matches(table_name,'\d+'))[1]::int
        ;
    '''
    d = Database(database_name='GRUKOS', usr='writer')
    with d.connection as con_pg:
        cur = con_pg.cursor()
        cur.execute(sql_tablename)
        tablenames = cur.fetchall()

    tablenames_edit = tablenames[:-1]
    num = len(tablenames_edit)

    for i, row in enumerate(tablenames_edit):

        j = i+1
        previous_tablename = row[0]
        tablename = tablenames[j][0]

        if i+1 == num:
            tablename_edit = '9999_dummy_layer_isopach'
        else:
            tablename_edit = f'{tablename}_isopach'

        print(f'Adding sql for isopach table {tablename_edit}...')
        sql_fohm_thickness = f"""
        -- table: {schema_name}.{tablename_edit}

        DROP TABLE IF EXISTS {schema_name}."{tablename_edit}";
        
        CREATE TABLE {schema_name}."{tablename_edit}" AS 
            (
                WITH 
                    l1 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM {schema_name}."{previous_tablename}"
                        ),
                    l2 AS 
                        (
                            SELECT st_union(rast) AS rast 
                            FROM {schema_name}."{tablename}"
                        ),
                    thick AS 
                        (
                            SELECT 
                                ST_MapAlgebra(l1.rast, l2.rast, '([rast1]-[rast2])') AS rast
                            FROM l1, l2
                        )
                SELECT 
                    ROW_NUMBER() OVER () AS rid,
                    rast 
                FROM thick
                ORDER BY rid        
            )
        ;

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE {schema_name}."{tablename_edit}" IS '''
                     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                     || ' user:simak '
                     || ' descr:Table containing the isopach map displaying the layer thickness of {tablename_edit}'
                     || ' repo:https://gitlab.com/mst-gko/pgfohm'
                     || '''';
            END
        $do$
        ;
        
        ALTER TABLE {schema_name}."{tablename_edit}" DROP CONSTRAINT IF EXISTS "{tablename_edit}_pkey";
        ALTER TABLE {schema_name}."{tablename_edit}" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "{tablename_edit}_rastidx";
        CREATE INDEX "{tablename_edit}_rastidx" ON {schema_name}."{tablename_edit}" USING gist(st_convexhull(rast));
        
        SELECT AddRasterConstraints('{schema_name}'::name, '{tablename_edit}'::name, 'rast'::name);

        """

        sql_fohm_thickness_combined += sql_fohm_thickness

sql_access = f'''
COMMIT;

GRANT SELECT ON ALL TABLES IN SCHEMA {schema_name} TO jupiterrole;
GRANT USAGE ON SCHEMA {schema_name} TO jupiterrole;
'''
sql_fohm_thickness_combined += sql_access

# export sql file
sql_file_name = f'fohm_isopach.sql'
with io.open(sql_file_name, 'w', encoding='utf8') as f:
    f.write(sql_fohm_thickness_combined)

# end timer
elapsed = round(time.time() - t, 2)
print(f'\n{os.path.basename(__file__)} executed in {elapsed} s')
