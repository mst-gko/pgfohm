BEGIN;

	DROP TABLE IF EXISTS fohm_borehole.fohm_filter_lith_all;

	CREATE TABLE fohm_borehole.fohm_filter_lith_all AS 
		(
			WITH 
				screen_ele AS 
					(
						SELECT DISTINCT ON (s.guid)
							s.boreholeid, 
							i.guid::uuid AS guid_intake, 
							s.guid::uuid AS guid_screen,
							i.intakeno, s.screenno, 
							s.top, s.bottom,
							b.use, dp.plantid, dp.plantname, ct.companytype,
							b.elevation-s.top AS top_ele, 
							b.elevation-s.bottom AS bottom_ele
						FROM jupiter_fdw.screen s
						INNER JOIN jupiter_fdw.borehole b USING (boreholeid)
						INNER JOIN jupiter_fdw.intake i ON s.guid_intake = i.guid 	
						LEFT JOIN jupiter_fdw.drwplantintake dpi ON i.guid = dpi.guid_intake 
						LEFT JOIN jupiter_fdw.drwplant dp ON dpi.plantid = dp.plantid 
						LEFT JOIN 
							(
								SELECT dpct.plantid, string_agg(dpct.companytype, ', ') AS companytype
								FROM jupiter_fdw.drwplantcompanytype dpct 
								GROUP BY dpct.plantid
							) AS ct ON dp.plantid = ct.plantid
					),
				filter_layer AS 
					(
						SELECT
							flj.boreholeid, s.guid_intake, guid_screen, 
							s.intakeno, s.screenno, 
							flj.layer, flj.layer_num, flj.litho, flj.time_period, 
							s.top_ele, flj.layer_top, s.bottom_ele, flj.layer_bot,
							s.use, s.plantid, s.plantname, s.companytype,
							CASE 
								WHEN s.top_ele < flj.layer_top 
									THEN s.top_ele 
								ELSE flj.layer_top 
								END AS filt_top_ele_adjusted,
							CASE 
								WHEN s.bottom_ele > flj.layer_bot 
									THEN s.bottom_ele 
								ELSE flj.layer_bot 
								END AS filt_bot_ele_adjusted,
							abs(s.top_ele - s.bottom_ele) AS filt_length,
							b.geom
						FROM fohm_borehole.fohm_layer_borehole flj
						INNER JOIN screen_ele s ON s.boreholeid = flj.boreholeid 
						INNER JOIN jupiter_fdw.borehole b ON b.boreholeid = flj.boreholeid 
						WHERE flj.layer_thick != 0
							AND flj.layer_top >= s.bottom_ele
							AND flj.layer_bot <= s.top_ele
					)
			SELECT DISTINCT
				fl.boreholeid,
				fl.guid_intake, fl.guid_screen,
				fl.intakeno, fl.screenno, 
				layer, layer_num, 
				litho, time_period,
				--lith_screen_agg AS jup_lith_scr,
				top_ele AS scr_top_ele, bottom_ele AS scr_bot_ele,
				layer_top AS fohm_top_ele, layer_bot AS fohm_bot_ele,
				round( 100 * (abs(filt_top_ele_adjusted - filt_bot_ele_adjusted)) / filt_length) AS intake_pct,
				fl.use, fl.plantid, fl.plantname, fl.companytype,
				geom
			FROM filter_layer fl
			LEFT JOIN jupiter_fdw.lith_in_borscreen lib ON lib.guid_screen = fl.guid_screen
			ORDER BY boreholeid DESC, guid_intake, intake_pct DESC NULLS LAST
		)
	;
	
	CREATE INDEX fohm_filter_lith_all_geom_idx
		ON fohm_borehole.fohm_filter_lith_all
	  	USING GIST (geom);
	  
	CREATE INDEX fohm_filter_lith_all_idx1
		ON fohm_borehole.fohm_filter_lith_all
	  	USING BTREE (boreholeid);
	 
	CREATE INDEX fohm_filter_lith_all_idx2
		ON fohm_borehole.fohm_filter_lith_all
	  	USING BTREE (guid_intake);
	  
	CREATE INDEX fohm_filter_lith_all_idx3
		ON fohm_borehole.fohm_filter_lith_all
	  	USING BTREE (guid_screen);
	  
	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE fohm_borehole.fohm_filter_lith_all IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' simak'
	     || ' Table containing all layers that is within the borehole filter interval'
	     || '''';
		END
	$do$
	;
	
	DROP TABLE IF EXISTS fohm_borehole.fohm_filter_lith_aquifer;
	CREATE TABLE fohm_borehole.fohm_filter_lith_aquifer AS 
		(
			SELECT DISTINCT ON (boreholeid, guid_screen) 
				*
			FROM fohm_borehole.fohm_filter_lith_all
			ORDER BY boreholeid, guid_screen, 
				CASE WHEN litho IN ('Sand', 'Limestone') THEN 1 ELSE 2 END,
				intake_pct DESC NULLS LAST 
		)
	;
	
	CREATE INDEX fohm_filter_lith_aquifer_geom_idx
		ON fohm_borehole.fohm_filter_lith_aquifer
	  	USING GIST (geom);
	  
	CREATE INDEX fohm_filter_lith_aquifer_idx1
		ON fohm_borehole.fohm_filter_lith_aquifer
	  	USING BTREE (boreholeid);
	 
	CREATE INDEX fohm_filter_lith_aquifer_idx2
		ON fohm_borehole.fohm_filter_lith_aquifer
	  	USING BTREE (guid_intake);
	  
	CREATE INDEX fohm_filter_lith_aquifer_idx3
		ON fohm_borehole.fohm_filter_lith_aquifer
	  	USING BTREE (guid_screen);
	  
	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE fohm_borehole.fohm_filter_lith_aquifer IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' simak'
	     || ' Table containing a single aquifer layer that is covering most of the borehole filter interval'
	     || '''';
		END
	$do$
	;

	GRANT USAGE ON SCHEMA fohm_borehole TO grukosreader;
	GRANT SELECT ON ALL TABLES IN SCHEMA fohm_borehole TO grukosreader;
	
COMMIT;
