import io
import configparser

# import psycopg2 as pg
import pandas as pd
from sqlalchemy import create_engine


class Database:
    """Class handles all database related function"""
    def __init__(self, database_name, usr):
        """
        Initiates the classe Database
        :param database_name: string used to specify which database name on MST-Grundvand server to connect to
        :param usr: string used to specify the user, e.g. reader og writer
        """
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    @property
    def parse_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: dict containing db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def create_postgis_engine(self):
        cred = self.parse_db_credentials
        engine = create_engine(
            f"postgresql://{cred['usr']}:{cred['pw']}@{cred['host']}:{cred['port']}/{cred['dbname']}"
        )
        del cred

        return engine

    def connect_to_pgsql_db(self):
        """
        Connects to postgresql server database
        :return: postgresql psycopg2 connection and string database name
        """
        engine = self.create_postgis_engine()
        pg_con = engine.connect()

        return pg_con

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query string for the import
        :return: pandas dataframe containing output from sql query
        """
        conn = self.connect_to_pgsql_db()
        try:
            # print(f'Fetching table from {database} to dataframe...')
            df_pgsql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {self.ini_database_name}')
        else:
            # print(f'Table loaded into dataframe from database: {database}')
            pass

        return df_pgsql


def fetch_fohm_layer_names_jylland():

    sql_fohm_jylland = f"""
        SELECT DISTINCT 
            layernumber AS layer_number,
            LEFT(filename, 4) AS layer_id, 
            'jylland_' || LEFT(filename, 4) || '_' || layername AS layer_name
        FROM fohm.fohm_grids
        WHERE modelnavn = 'jylland'
        ORDER BY LEFT(filename, 4)  
    """

    return sql_fohm_jylland


def fetch_fohm_layer_names_fyn():

    sql_fohm_fyn = f"""
        WITH tmp AS 
            (
                SELECT DISTINCT 
                    layernumber as layer_number,	
                    DENSE_RANK() OVER (ORDER BY  layernumber) AS layer_id, 
                    REPLACE(layername, 'fyn_', '') AS layer_name
                FROM fohm.fohm_grids
                WHERE modelnavn = 'fyn'
            )
        SELECT 
            layer_number,
            layer_id,
            'fyn_' || LPAD(layer_id::TEXT, 4, '0') || '_' || layer_name AS layer_name
        FROM tmp
        ORDER BY layer_number 
    """

    return sql_fohm_fyn


def fetch_fohm_layer_names_sjaelland():
    sql_fohm_sjaelland = f"""
        WITH tmp AS 
            (
                SELECT DISTINCT 
                    layernumber as layer_number,	
                    DENSE_RANK() OVER (ORDER BY  layernumber) AS layer_id, 
                    REPLACE(layername, 'sjaelland_', '') AS layer_name
                FROM fohm.fohm_grids
                WHERE modelnavn = 'sjælland'
            )
        SELECT 
            layer_number,
            layer_id,
            'sjaelland_' || LPAD(layer_id::TEXT, 4, '0') || '_' || layer_name AS layer_name
        FROM tmp
        ORDER BY layer_number   
    """

    return sql_fohm_sjaelland


def create_fohm_sql(model_name, sql_fohm_layer_names):
    db = Database(database_name='GRUKOS', usr='reader')
    df_fohm_layer_names = db.import_pgsql_to_df(sql=sql_fohm_layer_names)

    sql_restore_fohm = f"""
    BEGIN;
        
        CREATE SCHEMA IF NOT EXISTS mstfohm;
        
        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON SCHEMA mstfohm IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: all grids from fohm'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
    """

    for index, row in df_fohm_layer_names.iterrows():
        layer_number = row['layer_number']
        layer_name = row['layer_name']
        layer_name = layer_name.replace('ø', 'oe').replace('æ', 'ae').replace('å', 'aa').replace(' ', '_')
        layer_name = layer_name.lower()

        sql_table = f"""
        /*
        * table:{layer_name}
        */
       
        DROP TABLE IF EXISTS mstfohm."{layer_name}";
        
        CREATE TABLE mstfohm."{layer_name}" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = '{model_name}'
                    AND layernumber = {layer_number}
            )
        ;
        
        ALTER TABLE mstfohm."{layer_name}" DROP CONSTRAINT IF EXISTS "{layer_name}_pkey";
        ALTER TABLE mstfohm."{layer_name}" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "{layer_name}_rastidx";
        CREATE INDEX "{layer_name}_rastidx" ON mstfohm."{layer_name}" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."{layer_name}" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid {layer_name} from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, '{layer_name}'::name,'rast'::name);
        """

        sql_restore_fohm += sql_table

    sql_restore_fohm += f"""
    COMMIT;
    
    GRANT USAGE ON SCHEMA fohm TO grukosreader;
    GRANT SELECT ON ALL TABLES IN SCHEMA fohm TO grukosreader;
    
    GRANT USAGE ON SCHEMA mstfohm TO grukosreader;
    GRANT SELECT ON ALL TABLES IN SCHEMA mstfohm TO grukosreader;
    
    """

    # export sql file
    model_name = model_name.replace('æ', 'ae')
    sql_file_name = f'restore_fohm_{model_name}.sql'
    with io.open(sql_file_name, 'w', encoding='utf8') as f:
        f.write(sql_restore_fohm)


fohm_layers_jylland = fetch_fohm_layer_names_jylland()
create_fohm_sql('jylland', fohm_layers_jylland)

fohm_layers_fyn = fetch_fohm_layer_names_fyn()
create_fohm_sql('fyn', fohm_layers_fyn)

fohm_layers_sjaelland = fetch_fohm_layer_names_sjaelland()
create_fohm_sql('sjælland', fohm_layers_sjaelland)
