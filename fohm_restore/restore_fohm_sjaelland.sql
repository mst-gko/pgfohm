
    BEGIN;
        
        CREATE SCHEMA IF NOT EXISTS mstfohm;
        
        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON SCHEMA mstfohm IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: all grids from fohm'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
    
        /*
        * table:sjaelland_0001_topo
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0001_topo";
        
        CREATE TABLE mstfohm."sjaelland_0001_topo" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 1
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0001_topo" DROP CONSTRAINT IF EXISTS "sjaelland_0001_topo_pkey";
        ALTER TABLE mstfohm."sjaelland_0001_topo" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0001_topo_rastidx";
        CREATE INDEX "sjaelland_0001_topo_rastidx" ON mstfohm."sjaelland_0001_topo" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0001_topo" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0001_topo from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0001_topo'::name,'rast'::name);
        
        /*
        * table:sjaelland_0002_kl1
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0002_kl1";
        
        CREATE TABLE mstfohm."sjaelland_0002_kl1" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 2
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0002_kl1" DROP CONSTRAINT IF EXISTS "sjaelland_0002_kl1_pkey";
        ALTER TABLE mstfohm."sjaelland_0002_kl1" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0002_kl1_rastidx";
        CREATE INDEX "sjaelland_0002_kl1_rastidx" ON mstfohm."sjaelland_0002_kl1" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0002_kl1" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0002_kl1 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0002_kl1'::name,'rast'::name);
        
        /*
        * table:sjaelland_0003_ks1
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0003_ks1";
        
        CREATE TABLE mstfohm."sjaelland_0003_ks1" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 3
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0003_ks1" DROP CONSTRAINT IF EXISTS "sjaelland_0003_ks1_pkey";
        ALTER TABLE mstfohm."sjaelland_0003_ks1" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0003_ks1_rastidx";
        CREATE INDEX "sjaelland_0003_ks1_rastidx" ON mstfohm."sjaelland_0003_ks1" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0003_ks1" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0003_ks1 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0003_ks1'::name,'rast'::name);
        
        /*
        * table:sjaelland_0004_kl2
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0004_kl2";
        
        CREATE TABLE mstfohm."sjaelland_0004_kl2" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 4
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0004_kl2" DROP CONSTRAINT IF EXISTS "sjaelland_0004_kl2_pkey";
        ALTER TABLE mstfohm."sjaelland_0004_kl2" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0004_kl2_rastidx";
        CREATE INDEX "sjaelland_0004_kl2_rastidx" ON mstfohm."sjaelland_0004_kl2" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0004_kl2" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0004_kl2 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0004_kl2'::name,'rast'::name);
        
        /*
        * table:sjaelland_0005_ks2
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0005_ks2";
        
        CREATE TABLE mstfohm."sjaelland_0005_ks2" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 5
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0005_ks2" DROP CONSTRAINT IF EXISTS "sjaelland_0005_ks2_pkey";
        ALTER TABLE mstfohm."sjaelland_0005_ks2" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0005_ks2_rastidx";
        CREATE INDEX "sjaelland_0005_ks2_rastidx" ON mstfohm."sjaelland_0005_ks2" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0005_ks2" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0005_ks2 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0005_ks2'::name,'rast'::name);
        
        /*
        * table:sjaelland_0006_kl3
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0006_kl3";
        
        CREATE TABLE mstfohm."sjaelland_0006_kl3" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 6
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0006_kl3" DROP CONSTRAINT IF EXISTS "sjaelland_0006_kl3_pkey";
        ALTER TABLE mstfohm."sjaelland_0006_kl3" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0006_kl3_rastidx";
        CREATE INDEX "sjaelland_0006_kl3_rastidx" ON mstfohm."sjaelland_0006_kl3" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0006_kl3" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0006_kl3 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0006_kl3'::name,'rast'::name);
        
        /*
        * table:sjaelland_0007_ks3
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0007_ks3";
        
        CREATE TABLE mstfohm."sjaelland_0007_ks3" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 7
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0007_ks3" DROP CONSTRAINT IF EXISTS "sjaelland_0007_ks3_pkey";
        ALTER TABLE mstfohm."sjaelland_0007_ks3" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0007_ks3_rastidx";
        CREATE INDEX "sjaelland_0007_ks3_rastidx" ON mstfohm."sjaelland_0007_ks3" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0007_ks3" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0007_ks3 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0007_ks3'::name,'rast'::name);
        
        /*
        * table:sjaelland_0008_kl4
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0008_kl4";
        
        CREATE TABLE mstfohm."sjaelland_0008_kl4" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 8
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0008_kl4" DROP CONSTRAINT IF EXISTS "sjaelland_0008_kl4_pkey";
        ALTER TABLE mstfohm."sjaelland_0008_kl4" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0008_kl4_rastidx";
        CREATE INDEX "sjaelland_0008_kl4_rastidx" ON mstfohm."sjaelland_0008_kl4" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0008_kl4" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0008_kl4 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0008_kl4'::name,'rast'::name);
        
        /*
        * table:sjaelland_0009_ks4
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0009_ks4";
        
        CREATE TABLE mstfohm."sjaelland_0009_ks4" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 9
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0009_ks4" DROP CONSTRAINT IF EXISTS "sjaelland_0009_ks4_pkey";
        ALTER TABLE mstfohm."sjaelland_0009_ks4" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0009_ks4_rastidx";
        CREATE INDEX "sjaelland_0009_ks4_rastidx" ON mstfohm."sjaelland_0009_ks4" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0009_ks4" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0009_ks4 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0009_ks4'::name,'rast'::name);
        
        /*
        * table:sjaelland_0010_kl5
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0010_kl5";
        
        CREATE TABLE mstfohm."sjaelland_0010_kl5" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 10
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0010_kl5" DROP CONSTRAINT IF EXISTS "sjaelland_0010_kl5_pkey";
        ALTER TABLE mstfohm."sjaelland_0010_kl5" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0010_kl5_rastidx";
        CREATE INDEX "sjaelland_0010_kl5_rastidx" ON mstfohm."sjaelland_0010_kl5" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0010_kl5" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0010_kl5 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0010_kl5'::name,'rast'::name);
        
        /*
        * table:sjaelland_0011_pl1
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0011_pl1";
        
        CREATE TABLE mstfohm."sjaelland_0011_pl1" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 11
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0011_pl1" DROP CONSTRAINT IF EXISTS "sjaelland_0011_pl1_pkey";
        ALTER TABLE mstfohm."sjaelland_0011_pl1" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0011_pl1_rastidx";
        CREATE INDEX "sjaelland_0011_pl1_rastidx" ON mstfohm."sjaelland_0011_pl1" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0011_pl1" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0011_pl1 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0011_pl1'::name,'rast'::name);
        
        /*
        * table:sjaelland_0012_gk1b
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0012_gk1b";
        
        CREATE TABLE mstfohm."sjaelland_0012_gk1b" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 12
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0012_gk1b" DROP CONSTRAINT IF EXISTS "sjaelland_0012_gk1b_pkey";
        ALTER TABLE mstfohm."sjaelland_0012_gk1b" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0012_gk1b_rastidx";
        CREATE INDEX "sjaelland_0012_gk1b_rastidx" ON mstfohm."sjaelland_0012_gk1b" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0012_gk1b" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0012_gk1b from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0012_gk1b'::name,'rast'::name);
        
        /*
        * table:sjaelland_0013_dk1b
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0013_dk1b";
        
        CREATE TABLE mstfohm."sjaelland_0013_dk1b" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 13
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0013_dk1b" DROP CONSTRAINT IF EXISTS "sjaelland_0013_dk1b_pkey";
        ALTER TABLE mstfohm."sjaelland_0013_dk1b" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0013_dk1b_rastidx";
        CREATE INDEX "sjaelland_0013_dk1b_rastidx" ON mstfohm."sjaelland_0013_dk1b" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0013_dk1b" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0013_dk1b from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0013_dk1b'::name,'rast'::name);
        
        /*
        * table:sjaelland_0014_sk1
        */
       
        DROP TABLE IF EXISTS mstfohm."sjaelland_0014_sk1";
        
        CREATE TABLE mstfohm."sjaelland_0014_sk1" AS 	
            (
                SELECT 
                    rid, 
                    ST_SetSRID(rast, 25832) AS rast  
                FROM fohm.fohm_grids fg 
                WHERE modelnavn = 'sjælland'
                    AND layernumber = 14
            )
        ;
        
        ALTER TABLE mstfohm."sjaelland_0014_sk1" DROP CONSTRAINT IF EXISTS "sjaelland_0014_sk1_pkey";
        ALTER TABLE mstfohm."sjaelland_0014_sk1" ADD PRIMARY KEY (rid);
        DROP INDEX IF EXISTS "sjaelland_0014_sk1_rastidx";
        CREATE INDEX "sjaelland_0014_sk1_rastidx" ON mstfohm."sjaelland_0014_sk1" USING gist(st_convexhull(rast));

        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE mstfohm."sjaelland_0014_sk1" IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' | user: simak'
             || ' | descr: grid sjaelland_0014_sk1 from fohm jylland'
             || ' | repo: https://gitlab.com/mst-gko/pgfohm'
             || '''';
            END
        $do$
        ;
        
        SELECT AddRasterConstraints('mstfohm'::name, 'sjaelland_0014_sk1'::name,'rast'::name);
        
    COMMIT;
    
    GRANT USAGE ON SCHEMA fohm TO grukosreader;
    GRANT SELECT ON ALL TABLES IN SCHEMA fohm TO grukosreader;
    
    GRANT USAGE ON SCHEMA mstfohm TO grukosreader;
    GRANT SELECT ON ALL TABLES IN SCHEMA mstfohm TO grukosreader;
    
    